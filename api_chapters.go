/*
API.Bible

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

API version: 1.6.3
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"bytes"
	"context"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

// Linger please
var (
	_ context.Context
)

// ChaptersApiService ChaptersApi service
type ChaptersApiService service

type ApiGetAudioChapterRequest struct {
	ctx context.Context
	ApiService *ChaptersApiService
	audioBibleId string
	chapterId string
}


func (r ApiGetAudioChapterRequest) Execute() (*InlineResponse20013, *http.Response, error) {
	return r.ApiService.GetAudioChapterExecute(r)
}

/*
GetAudioChapter Method for GetAudioChapter

Gets a single `Chapter` object for a given `audioBibleId` and `chapterId`.
This AudioChapter object also includes an `resourceUrl` property with a HTTP URL
to the mp3 audio resource for the chapter.  The `resourceUrl` is unique per request
and expires in XX minutes.  The `expiresAt` property provides the Unix time value of
`resourceUrl` expiration.


 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param audioBibleId Id of Bible whose Chapter to fetch
 @param chapterId Id of the Chapter to fetch
 @return ApiGetAudioChapterRequest
*/
func (a *ChaptersApiService) GetAudioChapter(ctx context.Context, audioBibleId string, chapterId string) ApiGetAudioChapterRequest {
	return ApiGetAudioChapterRequest{
		ApiService: a,
		ctx: ctx,
		audioBibleId: audioBibleId,
		chapterId: chapterId,
	}
}

// Execute executes the request
//  @return InlineResponse20013
func (a *ChaptersApiService) GetAudioChapterExecute(r ApiGetAudioChapterRequest) (*InlineResponse20013, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodGet
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *InlineResponse20013
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ChaptersApiService.GetAudioChapter")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/v1/audio-bibles/{audioBibleId}/chapters/{chapterId}"
	localVarPath = strings.Replace(localVarPath, "{"+"audioBibleId"+"}", url.PathEscape(parameterToString(r.audioBibleId, "")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"chapterId"+"}", url.PathEscape(parameterToString(r.chapterId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"*/*"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	if r.ctx != nil {
		// API Key Authentication
		if auth, ok := r.ctx.Value(ContextAPIKeys).(map[string]APIKey); ok {
			if apiKey, ok := auth["ApiKeyAuth"]; ok {
				var key string
				if apiKey.Prefix != "" {
					key = apiKey.Prefix + " " + apiKey.Key
				} else {
					key = apiKey.Key
				}
				localVarHeaderParams["api-key"] = key
			}
		}
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetAudioChaptersRequest struct {
	ctx context.Context
	ApiService *ChaptersApiService
	audioBibleId string
	bookId string
}


func (r ApiGetAudioChaptersRequest) Execute() (*InlineResponse2004, *http.Response, error) {
	return r.ApiService.GetAudioChaptersExecute(r)
}

/*
GetAudioChapters Method for GetAudioChapters

Gets an array of `Chapter` objects for a given `audioBibleId` and `bookId`


 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param audioBibleId Id of Bible whose Chapters to fetch
 @param bookId Id of the Book whose Chapters to fetch
 @return ApiGetAudioChaptersRequest
*/
func (a *ChaptersApiService) GetAudioChapters(ctx context.Context, audioBibleId string, bookId string) ApiGetAudioChaptersRequest {
	return ApiGetAudioChaptersRequest{
		ApiService: a,
		ctx: ctx,
		audioBibleId: audioBibleId,
		bookId: bookId,
	}
}

// Execute executes the request
//  @return InlineResponse2004
func (a *ChaptersApiService) GetAudioChaptersExecute(r ApiGetAudioChaptersRequest) (*InlineResponse2004, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodGet
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *InlineResponse2004
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ChaptersApiService.GetAudioChapters")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/v1/audio-bibles/{audioBibleId}/books/{bookId}/chapters"
	localVarPath = strings.Replace(localVarPath, "{"+"audioBibleId"+"}", url.PathEscape(parameterToString(r.audioBibleId, "")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"bookId"+"}", url.PathEscape(parameterToString(r.bookId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"*/*"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	if r.ctx != nil {
		// API Key Authentication
		if auth, ok := r.ctx.Value(ContextAPIKeys).(map[string]APIKey); ok {
			if apiKey, ok := auth["ApiKeyAuth"]; ok {
				var key string
				if apiKey.Prefix != "" {
					key = apiKey.Prefix + " " + apiKey.Key
				} else {
					key = apiKey.Key
				}
				localVarHeaderParams["api-key"] = key
			}
		}
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetChapterRequest struct {
	ctx context.Context
	ApiService *ChaptersApiService
	bibleId string
	chapterId string
	contentType *string
	includeNotes *bool
	includeTitles *bool
	includeChapterNumbers *bool
	includeVerseNumbers *bool
	includeVerseSpans *bool
	parallels *string
}

// Content type to be returned in the content property.  Supported values are &#x60;html&#x60; (default), &#x60;json&#x60; (beta), and &#x60;text&#x60; (beta)
func (r ApiGetChapterRequest) ContentType(contentType string) ApiGetChapterRequest {
	r.contentType = &contentType
	return r
}
// Include footnotes in content
func (r ApiGetChapterRequest) IncludeNotes(includeNotes bool) ApiGetChapterRequest {
	r.includeNotes = &includeNotes
	return r
}
// Include section titles in content
func (r ApiGetChapterRequest) IncludeTitles(includeTitles bool) ApiGetChapterRequest {
	r.includeTitles = &includeTitles
	return r
}
// Include chapter numbers in content
func (r ApiGetChapterRequest) IncludeChapterNumbers(includeChapterNumbers bool) ApiGetChapterRequest {
	r.includeChapterNumbers = &includeChapterNumbers
	return r
}
// Include verse numbers in content.
func (r ApiGetChapterRequest) IncludeVerseNumbers(includeVerseNumbers bool) ApiGetChapterRequest {
	r.includeVerseNumbers = &includeVerseNumbers
	return r
}
// Include spans that wrap verse numbers and verse text for bible content.
func (r ApiGetChapterRequest) IncludeVerseSpans(includeVerseSpans bool) ApiGetChapterRequest {
	r.includeVerseSpans = &includeVerseSpans
	return r
}
// Comma delimited list of bibleIds to include
func (r ApiGetChapterRequest) Parallels(parallels string) ApiGetChapterRequest {
	r.parallels = &parallels
	return r
}

func (r ApiGetChapterRequest) Execute() (*InlineResponse2005, *http.Response, error) {
	return r.ApiService.GetChapterExecute(r)
}

/*
GetChapter Method for GetChapter

Gets a single `Chapter` object for a given `bibleId` and `chapterId`.
This Chapter object also includes an `content` property with all verses for the Chapter.


 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param bibleId Id of Bible whose Chapter to fetch
 @param chapterId Id of the Chapter to fetch
 @return ApiGetChapterRequest
*/
func (a *ChaptersApiService) GetChapter(ctx context.Context, bibleId string, chapterId string) ApiGetChapterRequest {
	return ApiGetChapterRequest{
		ApiService: a,
		ctx: ctx,
		bibleId: bibleId,
		chapterId: chapterId,
	}
}

// Execute executes the request
//  @return InlineResponse2005
func (a *ChaptersApiService) GetChapterExecute(r ApiGetChapterRequest) (*InlineResponse2005, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodGet
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *InlineResponse2005
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ChaptersApiService.GetChapter")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/v1/bibles/{bibleId}/chapters/{chapterId}"
	localVarPath = strings.Replace(localVarPath, "{"+"bibleId"+"}", url.PathEscape(parameterToString(r.bibleId, "")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"chapterId"+"}", url.PathEscape(parameterToString(r.chapterId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if r.contentType != nil {
		localVarQueryParams.Add("content-type", parameterToString(*r.contentType, ""))
	}
	if r.includeNotes != nil {
		localVarQueryParams.Add("include-notes", parameterToString(*r.includeNotes, ""))
	}
	if r.includeTitles != nil {
		localVarQueryParams.Add("include-titles", parameterToString(*r.includeTitles, ""))
	}
	if r.includeChapterNumbers != nil {
		localVarQueryParams.Add("include-chapter-numbers", parameterToString(*r.includeChapterNumbers, ""))
	}
	if r.includeVerseNumbers != nil {
		localVarQueryParams.Add("include-verse-numbers", parameterToString(*r.includeVerseNumbers, ""))
	}
	if r.includeVerseSpans != nil {
		localVarQueryParams.Add("include-verse-spans", parameterToString(*r.includeVerseSpans, ""))
	}
	if r.parallels != nil {
		localVarQueryParams.Add("parallels", parameterToString(*r.parallels, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"*/*"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	if r.ctx != nil {
		// API Key Authentication
		if auth, ok := r.ctx.Value(ContextAPIKeys).(map[string]APIKey); ok {
			if apiKey, ok := auth["ApiKeyAuth"]; ok {
				var key string
				if apiKey.Prefix != "" {
					key = apiKey.Prefix + " " + apiKey.Key
				} else {
					key = apiKey.Key
				}
				localVarHeaderParams["api-key"] = key
			}
		}
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetChaptersRequest struct {
	ctx context.Context
	ApiService *ChaptersApiService
	bibleId string
	bookId string
}


func (r ApiGetChaptersRequest) Execute() (*InlineResponse2004, *http.Response, error) {
	return r.ApiService.GetChaptersExecute(r)
}

/*
GetChapters Method for GetChapters

Gets an array of `Chapter` objects for a given `bibleId` and `bookId`


 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param bibleId Id of Bible whose Chapters to fetch
 @param bookId Id of the Book whose Chapters to fetch
 @return ApiGetChaptersRequest
*/
func (a *ChaptersApiService) GetChapters(ctx context.Context, bibleId string, bookId string) ApiGetChaptersRequest {
	return ApiGetChaptersRequest{
		ApiService: a,
		ctx: ctx,
		bibleId: bibleId,
		bookId: bookId,
	}
}

// Execute executes the request
//  @return InlineResponse2004
func (a *ChaptersApiService) GetChaptersExecute(r ApiGetChaptersRequest) (*InlineResponse2004, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodGet
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *InlineResponse2004
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ChaptersApiService.GetChapters")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/v1/bibles/{bibleId}/books/{bookId}/chapters"
	localVarPath = strings.Replace(localVarPath, "{"+"bibleId"+"}", url.PathEscape(parameterToString(r.bibleId, "")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"bookId"+"}", url.PathEscape(parameterToString(r.bookId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"*/*"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	if r.ctx != nil {
		// API Key Authentication
		if auth, ok := r.ctx.Value(ContextAPIKeys).(map[string]APIKey); ok {
			if apiKey, ok := auth["ApiKeyAuth"]; ok {
				var key string
				if apiKey.Prefix != "" {
					key = apiKey.Prefix + " " + apiKey.Key
				} else {
					key = apiKey.Key
				}
				localVarHeaderParams["api-key"] = key
			}
		}
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
