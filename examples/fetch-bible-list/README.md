# Example: Fetch Bible List

All examples require an API key to be stored in the `API_BIBLE_API_KEY`
environment variable.  API keys can be created on the
[API.Bible](https://scripture.api.bible) webpage.

In this most basic example, we simply get a list of all Bibles available to the
API token.

```golang
package main

import (
	"context"
	"fmt"
	"os"

	bibleapi "gitlab.com/martyros/go-api-bible"
)

const API_KEY_ENV_VAR = "API_BIBLE_API_KEY"

func main() {
	conf := bibleapi.NewConfiguration()
	bible := bibleapi.NewAPIClient(conf)
	ctx := context.Background()

	if key, prs := os.LookupEnv(API_KEY_ENV_VAR); prs {
		ctx = context.WithValue(context.Background(),
			bibleapi.ContextAPIKeys,
			map[string]bibleapi.APIKey{
				"ApiKeyAuth": {
					Key: key,
				},
			})
	} else {
		fmt.Printf("No api key (%s not set), using unauthenticated requests\n", API_KEY_ENV_VAR)
	}

	list, r, err := bible.BiblesApi.GetBibles(ctx).Execute()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error when calling GetBibles: %v\n", err)
		fmt.Fprintf(os.Stderr, "Full HTTP Response: %v=n", r)
		os.Exit(1)
	}

	for _, v := range list.Data {
		fmt.Printf("Name: %v\n - Abbreviation: %v\n", v.Name, v.Abbreviation)
	}
}
```