# Example: Fetch John 3:16

All examples require an API key to be stored in the `API_BIBLE_API_KEY`
environment variable.  API keys can be created on the
[API.Bible](https://scripture.api.bible) webpage.

In this example, we fetch the verse "John 3:16" by walking the "tree", from
Bible -> Books -> Chapters -> Verses.

NB the easier way to do this would be to use the Search function; this is in
part to demonstrate how the API might work.

```golang
package main

import (
	"context"
	"fmt"
	"log"
	"os"

	bibleapi "gitlab.com/martyros/go-api-bible"
)

const API_KEY_ENV_VAR = "API_BIBLE_API_KEY"

const (
	BIBLEAPI_ABBREVIATION_WEB = "WEB"
)

func main() {
	bookName := "John"
	chapterNo := "3"
	verseNo := 16

	conf := bibleapi.NewConfiguration()
	bible := bibleapi.NewAPIClient(conf)
	ctx := context.Background()

	if key, prs := os.LookupEnv(API_KEY_ENV_VAR); prs {
		ctx = context.WithValue(context.Background(),
			bibleapi.ContextAPIKeys,
			map[string]bibleapi.APIKey{
				"ApiKeyAuth": {
					Key: key,
				},
			})
	} else {
		fmt.Printf("No api key (%s not set), using unauthenticated requests\n", API_KEY_ENV_VAR)
	}

	// Get the Bible ID
	var bibleId string
	if rsp, _, err := bible.BiblesApi.GetBibles(ctx).Abbreviation(BIBLEAPI_ABBREVIATION_WEB).Execute(); err != nil {
		log.Fatalf("Error getting Bible ID for %v: %v\n", BIBLEAPI_ABBREVIATION_WEB, err)
	} else if len(rsp.Data) < 1 {
		log.Fatalf("No data in response!")
	} else {
		bibleId = rsp.Data[0].Id
	}
	log.Printf("Bible ID: %v", bibleId)

	// Get the Book ID and Chapter id.  Doing both uses more bandwidth (since
	// we're getting all the chapters in the Bible, rather than just the book of
	// interest), but means one less round trip.
	var chapterId string
	if rsp, _, err := bible.BooksApi.GetBooks(ctx, bibleId).IncludeChapters(true).Execute(); err != nil {
		log.Fatalf("Error getting books / chapters for %v: %v\n", bibleId, err)
	} else {
	outer:
		for _, book := range rsp.Data {
			log.Printf("Checking book %v [%v] (%v)", book.Name, book.Abbreviation, book.NameLong)
			if book.Name == bookName {
				log.Printf("BookId: %v", book.Id)

				for _, chapter := range book.Chapters {
					log.Printf("Checking chapter %v (%v)", chapter.Number, chapter.Reference)
					if chapter.Number == chapterNo {
						chapterId = chapter.Id
						break outer
					}
				}
			}
		}

		if chapterId == "" {
			log.Fatalf("Couldn't find book %s chapter %s!", bookName, chapterNo)
		}
	}

	log.Printf("Chapter id: %v", chapterId)

	// Get the Verse ID
	var verseId string
	if rsp, _, err := bible.VersesApi.GetVerses(ctx, bibleId, chapterId).Execute(); err != nil {
		log.Fatalf("Error getting verses for bibleID %s chapterId %s: %v", bibleId, chapterId, err)
	} else if len(rsp.Data) < verseNo {
		log.Fatalf("Number of verses %v less than requested verse %v", len(rsp.Data), verseNo)
	} else {
		verseId = rsp.Data[verseNo-1].Id
	}

	log.Printf("Verse id: %v", verseId)

	// Get the Verse
	if rsp, _, err := bible.VersesApi.GetVerse(ctx, bibleId, verseId).ContentType("text").IncludeVerseNumbers(false).Execute(); err != nil {
		log.Fatalf("Error getting verse content for bibleID %s verseId %s: %v", bibleId, verseId, err)
	} else {
		fmt.Printf(rsp.Data.Content)
	}
}
```