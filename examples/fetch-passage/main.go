package main

import (
	"context"
	"fmt"
	"log"
	"os"

	bibleapi "gitlab.com/martyros/go-api-bible"
)

const API_KEY_ENV_VAR = "API_BIBLE_API_KEY"

const (
	BIBLEAPI_ABBREVIATION_WEB = "WEB"
)

func main() {
	// NB It's not clear from the APIs how "stable" this format is.  It might be
	// safer to use the Search api to convert a convenstional passage like "John
	// 3:16-18" into a passageId
	var passageId = "JHN.3.16-JHN.3.18"

	conf := bibleapi.NewConfiguration()
	bible := bibleapi.NewAPIClient(conf)
	ctx := context.Background()

	if key, prs := os.LookupEnv(API_KEY_ENV_VAR); prs {
		ctx = context.WithValue(context.Background(),
			bibleapi.ContextAPIKeys,
			map[string]bibleapi.APIKey{
				"ApiKeyAuth": {
					Key: key,
				},
			})
	} else {
		fmt.Printf("No api key (%s not set), using unauthenticated requests\n", API_KEY_ENV_VAR)
	}

	// Get the Bible ID
	var bibleId string
	if rsp, _, err := bible.BiblesApi.GetBibles(ctx).Abbreviation(BIBLEAPI_ABBREVIATION_WEB).Execute(); err != nil {
		log.Fatalf("Error getting Bible ID for %v: %v\n", BIBLEAPI_ABBREVIATION_WEB, err)
	} else if len(rsp.Data) < 1 {
		log.Fatalf("No data in response!")
	} else {
		bibleId = rsp.Data[0].Id
	}
	log.Printf("Bible ID: %v", bibleId)

	if rsp, r, err := bible.PassagesApi.GetPassage(ctx, bibleId, passageId).ContentType("text").IncludeVerseNumbers(false).Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "Error when calling GetPassage: %v\n", err)
		fmt.Fprintf(os.Stderr, "Full HTTP Response: %v=n", r)
		os.Exit(1)
	} else {
		fmt.Printf(rsp.Data.Content)
	}
}
