/*
API.Bible

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

API version: 1.6.3
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// SearchResponse struct for SearchResponse
type SearchResponse struct {
	Query string `json:"query"`
	Limit int32 `json:"limit"`
	Offset int32 `json:"offset"`
	Total int32 `json:"total"`
	VerseCount int32 `json:"verseCount"`
	Verses []SearchVerse `json:"verses"`
	Passages []Passage `json:"passages,omitempty"`
}

// NewSearchResponse instantiates a new SearchResponse object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewSearchResponse(query string, limit int32, offset int32, total int32, verseCount int32, verses []SearchVerse) *SearchResponse {
	this := SearchResponse{}
	this.Query = query
	this.Limit = limit
	this.Offset = offset
	this.Total = total
	this.VerseCount = verseCount
	this.Verses = verses
	return &this
}

// NewSearchResponseWithDefaults instantiates a new SearchResponse object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewSearchResponseWithDefaults() *SearchResponse {
	this := SearchResponse{}
	return &this
}

// GetQuery returns the Query field value
func (o *SearchResponse) GetQuery() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Query
}

// GetQueryOk returns a tuple with the Query field value
// and a boolean to check if the value has been set.
func (o *SearchResponse) GetQueryOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Query, true
}

// SetQuery sets field value
func (o *SearchResponse) SetQuery(v string) {
	o.Query = v
}

// GetLimit returns the Limit field value
func (o *SearchResponse) GetLimit() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.Limit
}

// GetLimitOk returns a tuple with the Limit field value
// and a boolean to check if the value has been set.
func (o *SearchResponse) GetLimitOk() (*int32, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Limit, true
}

// SetLimit sets field value
func (o *SearchResponse) SetLimit(v int32) {
	o.Limit = v
}

// GetOffset returns the Offset field value
func (o *SearchResponse) GetOffset() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.Offset
}

// GetOffsetOk returns a tuple with the Offset field value
// and a boolean to check if the value has been set.
func (o *SearchResponse) GetOffsetOk() (*int32, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Offset, true
}

// SetOffset sets field value
func (o *SearchResponse) SetOffset(v int32) {
	o.Offset = v
}

// GetTotal returns the Total field value
func (o *SearchResponse) GetTotal() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.Total
}

// GetTotalOk returns a tuple with the Total field value
// and a boolean to check if the value has been set.
func (o *SearchResponse) GetTotalOk() (*int32, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Total, true
}

// SetTotal sets field value
func (o *SearchResponse) SetTotal(v int32) {
	o.Total = v
}

// GetVerseCount returns the VerseCount field value
func (o *SearchResponse) GetVerseCount() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.VerseCount
}

// GetVerseCountOk returns a tuple with the VerseCount field value
// and a boolean to check if the value has been set.
func (o *SearchResponse) GetVerseCountOk() (*int32, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.VerseCount, true
}

// SetVerseCount sets field value
func (o *SearchResponse) SetVerseCount(v int32) {
	o.VerseCount = v
}

// GetVerses returns the Verses field value
func (o *SearchResponse) GetVerses() []SearchVerse {
	if o == nil {
		var ret []SearchVerse
		return ret
	}

	return o.Verses
}

// GetVersesOk returns a tuple with the Verses field value
// and a boolean to check if the value has been set.
func (o *SearchResponse) GetVersesOk() ([]SearchVerse, bool) {
	if o == nil  {
		return nil, false
	}
	return o.Verses, true
}

// SetVerses sets field value
func (o *SearchResponse) SetVerses(v []SearchVerse) {
	o.Verses = v
}

// GetPassages returns the Passages field value if set, zero value otherwise.
func (o *SearchResponse) GetPassages() []Passage {
	if o == nil || o.Passages == nil {
		var ret []Passage
		return ret
	}
	return o.Passages
}

// GetPassagesOk returns a tuple with the Passages field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SearchResponse) GetPassagesOk() ([]Passage, bool) {
	if o == nil || o.Passages == nil {
		return nil, false
	}
	return o.Passages, true
}

// HasPassages returns a boolean if a field has been set.
func (o *SearchResponse) HasPassages() bool {
	if o != nil && o.Passages != nil {
		return true
	}

	return false
}

// SetPassages gets a reference to the given []Passage and assigns it to the Passages field.
func (o *SearchResponse) SetPassages(v []Passage) {
	o.Passages = v
}

func (o SearchResponse) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["query"] = o.Query
	}
	if true {
		toSerialize["limit"] = o.Limit
	}
	if true {
		toSerialize["offset"] = o.Offset
	}
	if true {
		toSerialize["total"] = o.Total
	}
	if true {
		toSerialize["verseCount"] = o.VerseCount
	}
	if true {
		toSerialize["verses"] = o.Verses
	}
	if o.Passages != nil {
		toSerialize["passages"] = o.Passages
	}
	return json.Marshal(toSerialize)
}

type NullableSearchResponse struct {
	value *SearchResponse
	isSet bool
}

func (v NullableSearchResponse) Get() *SearchResponse {
	return v.value
}

func (v *NullableSearchResponse) Set(val *SearchResponse) {
	v.value = val
	v.isSet = true
}

func (v NullableSearchResponse) IsSet() bool {
	return v.isSet
}

func (v *NullableSearchResponse) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableSearchResponse(val *SearchResponse) *NullableSearchResponse {
	return &NullableSearchResponse{value: val, isSet: true}
}

func (v NullableSearchResponse) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableSearchResponse) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


