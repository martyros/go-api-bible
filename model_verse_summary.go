/*
API.Bible

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

API version: 1.6.3
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// VerseSummary struct for VerseSummary
type VerseSummary struct {
	Id string `json:"id"`
	OrgId string `json:"orgId"`
	BibleId string `json:"bibleId"`
	BookId string `json:"bookId"`
	ChapterId string `json:"chapterId"`
	Reference string `json:"reference"`
}

// NewVerseSummary instantiates a new VerseSummary object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewVerseSummary(id string, orgId string, bibleId string, bookId string, chapterId string, reference string) *VerseSummary {
	this := VerseSummary{}
	this.Id = id
	this.OrgId = orgId
	this.BibleId = bibleId
	this.BookId = bookId
	this.ChapterId = chapterId
	this.Reference = reference
	return &this
}

// NewVerseSummaryWithDefaults instantiates a new VerseSummary object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewVerseSummaryWithDefaults() *VerseSummary {
	this := VerseSummary{}
	return &this
}

// GetId returns the Id field value
func (o *VerseSummary) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *VerseSummary) GetIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *VerseSummary) SetId(v string) {
	o.Id = v
}

// GetOrgId returns the OrgId field value
func (o *VerseSummary) GetOrgId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.OrgId
}

// GetOrgIdOk returns a tuple with the OrgId field value
// and a boolean to check if the value has been set.
func (o *VerseSummary) GetOrgIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.OrgId, true
}

// SetOrgId sets field value
func (o *VerseSummary) SetOrgId(v string) {
	o.OrgId = v
}

// GetBibleId returns the BibleId field value
func (o *VerseSummary) GetBibleId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.BibleId
}

// GetBibleIdOk returns a tuple with the BibleId field value
// and a boolean to check if the value has been set.
func (o *VerseSummary) GetBibleIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.BibleId, true
}

// SetBibleId sets field value
func (o *VerseSummary) SetBibleId(v string) {
	o.BibleId = v
}

// GetBookId returns the BookId field value
func (o *VerseSummary) GetBookId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.BookId
}

// GetBookIdOk returns a tuple with the BookId field value
// and a boolean to check if the value has been set.
func (o *VerseSummary) GetBookIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.BookId, true
}

// SetBookId sets field value
func (o *VerseSummary) SetBookId(v string) {
	o.BookId = v
}

// GetChapterId returns the ChapterId field value
func (o *VerseSummary) GetChapterId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ChapterId
}

// GetChapterIdOk returns a tuple with the ChapterId field value
// and a boolean to check if the value has been set.
func (o *VerseSummary) GetChapterIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.ChapterId, true
}

// SetChapterId sets field value
func (o *VerseSummary) SetChapterId(v string) {
	o.ChapterId = v
}

// GetReference returns the Reference field value
func (o *VerseSummary) GetReference() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Reference
}

// GetReferenceOk returns a tuple with the Reference field value
// and a boolean to check if the value has been set.
func (o *VerseSummary) GetReferenceOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Reference, true
}

// SetReference sets field value
func (o *VerseSummary) SetReference(v string) {
	o.Reference = v
}

func (o VerseSummary) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["id"] = o.Id
	}
	if true {
		toSerialize["orgId"] = o.OrgId
	}
	if true {
		toSerialize["bibleId"] = o.BibleId
	}
	if true {
		toSerialize["bookId"] = o.BookId
	}
	if true {
		toSerialize["chapterId"] = o.ChapterId
	}
	if true {
		toSerialize["reference"] = o.Reference
	}
	return json.Marshal(toSerialize)
}

type NullableVerseSummary struct {
	value *VerseSummary
	isSet bool
}

func (v NullableVerseSummary) Get() *VerseSummary {
	return v.value
}

func (v *NullableVerseSummary) Set(val *VerseSummary) {
	v.value = val
	v.isSet = true
}

func (v NullableVerseSummary) IsSet() bool {
	return v.isSet
}

func (v *NullableVerseSummary) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableVerseSummary(val *VerseSummary) *NullableVerseSummary {
	return &NullableVerseSummary{value: val, isSet: true}
}

func (v NullableVerseSummary) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableVerseSummary) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


