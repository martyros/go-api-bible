/*
API.Bible

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

API version: 1.6.3
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// AudioChapter struct for AudioChapter
type AudioChapter struct {
	Id string `json:"id"`
	BibleId string `json:"bibleId"`
	Number string `json:"number"`
	BookId string `json:"bookId"`
	ResourceUrl string `json:"resourceUrl"`
	Timecodes []AudioChapterTimecodes `json:"timecodes,omitempty"`
	ExpiresAt int32 `json:"expiresAt"`
	Reference string `json:"reference"`
	Next *ChapterNext `json:"next,omitempty"`
	Previous *ChapterNext `json:"previous,omitempty"`
	Copyright *string `json:"copyright,omitempty"`
}

// NewAudioChapter instantiates a new AudioChapter object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAudioChapter(id string, bibleId string, number string, bookId string, resourceUrl string, expiresAt int32, reference string) *AudioChapter {
	this := AudioChapter{}
	this.Id = id
	this.BibleId = bibleId
	this.Number = number
	this.BookId = bookId
	this.ResourceUrl = resourceUrl
	this.ExpiresAt = expiresAt
	this.Reference = reference
	return &this
}

// NewAudioChapterWithDefaults instantiates a new AudioChapter object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAudioChapterWithDefaults() *AudioChapter {
	this := AudioChapter{}
	return &this
}

// GetId returns the Id field value
func (o *AudioChapter) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *AudioChapter) GetIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *AudioChapter) SetId(v string) {
	o.Id = v
}

// GetBibleId returns the BibleId field value
func (o *AudioChapter) GetBibleId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.BibleId
}

// GetBibleIdOk returns a tuple with the BibleId field value
// and a boolean to check if the value has been set.
func (o *AudioChapter) GetBibleIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.BibleId, true
}

// SetBibleId sets field value
func (o *AudioChapter) SetBibleId(v string) {
	o.BibleId = v
}

// GetNumber returns the Number field value
func (o *AudioChapter) GetNumber() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Number
}

// GetNumberOk returns a tuple with the Number field value
// and a boolean to check if the value has been set.
func (o *AudioChapter) GetNumberOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Number, true
}

// SetNumber sets field value
func (o *AudioChapter) SetNumber(v string) {
	o.Number = v
}

// GetBookId returns the BookId field value
func (o *AudioChapter) GetBookId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.BookId
}

// GetBookIdOk returns a tuple with the BookId field value
// and a boolean to check if the value has been set.
func (o *AudioChapter) GetBookIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.BookId, true
}

// SetBookId sets field value
func (o *AudioChapter) SetBookId(v string) {
	o.BookId = v
}

// GetResourceUrl returns the ResourceUrl field value
func (o *AudioChapter) GetResourceUrl() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ResourceUrl
}

// GetResourceUrlOk returns a tuple with the ResourceUrl field value
// and a boolean to check if the value has been set.
func (o *AudioChapter) GetResourceUrlOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.ResourceUrl, true
}

// SetResourceUrl sets field value
func (o *AudioChapter) SetResourceUrl(v string) {
	o.ResourceUrl = v
}

// GetTimecodes returns the Timecodes field value if set, zero value otherwise.
func (o *AudioChapter) GetTimecodes() []AudioChapterTimecodes {
	if o == nil || o.Timecodes == nil {
		var ret []AudioChapterTimecodes
		return ret
	}
	return o.Timecodes
}

// GetTimecodesOk returns a tuple with the Timecodes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AudioChapter) GetTimecodesOk() ([]AudioChapterTimecodes, bool) {
	if o == nil || o.Timecodes == nil {
		return nil, false
	}
	return o.Timecodes, true
}

// HasTimecodes returns a boolean if a field has been set.
func (o *AudioChapter) HasTimecodes() bool {
	if o != nil && o.Timecodes != nil {
		return true
	}

	return false
}

// SetTimecodes gets a reference to the given []AudioChapterTimecodes and assigns it to the Timecodes field.
func (o *AudioChapter) SetTimecodes(v []AudioChapterTimecodes) {
	o.Timecodes = v
}

// GetExpiresAt returns the ExpiresAt field value
func (o *AudioChapter) GetExpiresAt() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.ExpiresAt
}

// GetExpiresAtOk returns a tuple with the ExpiresAt field value
// and a boolean to check if the value has been set.
func (o *AudioChapter) GetExpiresAtOk() (*int32, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.ExpiresAt, true
}

// SetExpiresAt sets field value
func (o *AudioChapter) SetExpiresAt(v int32) {
	o.ExpiresAt = v
}

// GetReference returns the Reference field value
func (o *AudioChapter) GetReference() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Reference
}

// GetReferenceOk returns a tuple with the Reference field value
// and a boolean to check if the value has been set.
func (o *AudioChapter) GetReferenceOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Reference, true
}

// SetReference sets field value
func (o *AudioChapter) SetReference(v string) {
	o.Reference = v
}

// GetNext returns the Next field value if set, zero value otherwise.
func (o *AudioChapter) GetNext() ChapterNext {
	if o == nil || o.Next == nil {
		var ret ChapterNext
		return ret
	}
	return *o.Next
}

// GetNextOk returns a tuple with the Next field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AudioChapter) GetNextOk() (*ChapterNext, bool) {
	if o == nil || o.Next == nil {
		return nil, false
	}
	return o.Next, true
}

// HasNext returns a boolean if a field has been set.
func (o *AudioChapter) HasNext() bool {
	if o != nil && o.Next != nil {
		return true
	}

	return false
}

// SetNext gets a reference to the given ChapterNext and assigns it to the Next field.
func (o *AudioChapter) SetNext(v ChapterNext) {
	o.Next = &v
}

// GetPrevious returns the Previous field value if set, zero value otherwise.
func (o *AudioChapter) GetPrevious() ChapterNext {
	if o == nil || o.Previous == nil {
		var ret ChapterNext
		return ret
	}
	return *o.Previous
}

// GetPreviousOk returns a tuple with the Previous field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AudioChapter) GetPreviousOk() (*ChapterNext, bool) {
	if o == nil || o.Previous == nil {
		return nil, false
	}
	return o.Previous, true
}

// HasPrevious returns a boolean if a field has been set.
func (o *AudioChapter) HasPrevious() bool {
	if o != nil && o.Previous != nil {
		return true
	}

	return false
}

// SetPrevious gets a reference to the given ChapterNext and assigns it to the Previous field.
func (o *AudioChapter) SetPrevious(v ChapterNext) {
	o.Previous = &v
}

// GetCopyright returns the Copyright field value if set, zero value otherwise.
func (o *AudioChapter) GetCopyright() string {
	if o == nil || o.Copyright == nil {
		var ret string
		return ret
	}
	return *o.Copyright
}

// GetCopyrightOk returns a tuple with the Copyright field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AudioChapter) GetCopyrightOk() (*string, bool) {
	if o == nil || o.Copyright == nil {
		return nil, false
	}
	return o.Copyright, true
}

// HasCopyright returns a boolean if a field has been set.
func (o *AudioChapter) HasCopyright() bool {
	if o != nil && o.Copyright != nil {
		return true
	}

	return false
}

// SetCopyright gets a reference to the given string and assigns it to the Copyright field.
func (o *AudioChapter) SetCopyright(v string) {
	o.Copyright = &v
}

func (o AudioChapter) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["id"] = o.Id
	}
	if true {
		toSerialize["bibleId"] = o.BibleId
	}
	if true {
		toSerialize["number"] = o.Number
	}
	if true {
		toSerialize["bookId"] = o.BookId
	}
	if true {
		toSerialize["resourceUrl"] = o.ResourceUrl
	}
	if o.Timecodes != nil {
		toSerialize["timecodes"] = o.Timecodes
	}
	if true {
		toSerialize["expiresAt"] = o.ExpiresAt
	}
	if true {
		toSerialize["reference"] = o.Reference
	}
	if o.Next != nil {
		toSerialize["next"] = o.Next
	}
	if o.Previous != nil {
		toSerialize["previous"] = o.Previous
	}
	if o.Copyright != nil {
		toSerialize["copyright"] = o.Copyright
	}
	return json.Marshal(toSerialize)
}

type NullableAudioChapter struct {
	value *AudioChapter
	isSet bool
}

func (v NullableAudioChapter) Get() *AudioChapter {
	return v.value
}

func (v *NullableAudioChapter) Set(val *AudioChapter) {
	v.value = val
	v.isSet = true
}

func (v NullableAudioChapter) IsSet() bool {
	return v.isSet
}

func (v *NullableAudioChapter) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAudioChapter(val *AudioChapter) *NullableAudioChapter {
	return &NullableAudioChapter{value: val, isSet: true}
}

func (v NullableAudioChapter) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAudioChapter) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


