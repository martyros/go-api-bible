/*
API.Bible

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

API version: 1.6.3
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// Chapter struct for Chapter
type Chapter struct {
	Id string `json:"id"`
	BibleId string `json:"bibleId"`
	Number string `json:"number"`
	BookId string `json:"bookId"`
	Content string `json:"content"`
	Reference string `json:"reference"`
	VerseCount int32 `json:"verseCount"`
	Next *ChapterNext `json:"next,omitempty"`
	Previous *ChapterNext `json:"previous,omitempty"`
	Copyright string `json:"copyright"`
}

// NewChapter instantiates a new Chapter object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewChapter(id string, bibleId string, number string, bookId string, content string, reference string, verseCount int32, copyright string) *Chapter {
	this := Chapter{}
	this.Id = id
	this.BibleId = bibleId
	this.Number = number
	this.BookId = bookId
	this.Content = content
	this.Reference = reference
	this.VerseCount = verseCount
	this.Copyright = copyright
	return &this
}

// NewChapterWithDefaults instantiates a new Chapter object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewChapterWithDefaults() *Chapter {
	this := Chapter{}
	return &this
}

// GetId returns the Id field value
func (o *Chapter) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *Chapter) GetIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *Chapter) SetId(v string) {
	o.Id = v
}

// GetBibleId returns the BibleId field value
func (o *Chapter) GetBibleId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.BibleId
}

// GetBibleIdOk returns a tuple with the BibleId field value
// and a boolean to check if the value has been set.
func (o *Chapter) GetBibleIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.BibleId, true
}

// SetBibleId sets field value
func (o *Chapter) SetBibleId(v string) {
	o.BibleId = v
}

// GetNumber returns the Number field value
func (o *Chapter) GetNumber() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Number
}

// GetNumberOk returns a tuple with the Number field value
// and a boolean to check if the value has been set.
func (o *Chapter) GetNumberOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Number, true
}

// SetNumber sets field value
func (o *Chapter) SetNumber(v string) {
	o.Number = v
}

// GetBookId returns the BookId field value
func (o *Chapter) GetBookId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.BookId
}

// GetBookIdOk returns a tuple with the BookId field value
// and a boolean to check if the value has been set.
func (o *Chapter) GetBookIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.BookId, true
}

// SetBookId sets field value
func (o *Chapter) SetBookId(v string) {
	o.BookId = v
}

// GetContent returns the Content field value
func (o *Chapter) GetContent() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Content
}

// GetContentOk returns a tuple with the Content field value
// and a boolean to check if the value has been set.
func (o *Chapter) GetContentOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Content, true
}

// SetContent sets field value
func (o *Chapter) SetContent(v string) {
	o.Content = v
}

// GetReference returns the Reference field value
func (o *Chapter) GetReference() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Reference
}

// GetReferenceOk returns a tuple with the Reference field value
// and a boolean to check if the value has been set.
func (o *Chapter) GetReferenceOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Reference, true
}

// SetReference sets field value
func (o *Chapter) SetReference(v string) {
	o.Reference = v
}

// GetVerseCount returns the VerseCount field value
func (o *Chapter) GetVerseCount() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.VerseCount
}

// GetVerseCountOk returns a tuple with the VerseCount field value
// and a boolean to check if the value has been set.
func (o *Chapter) GetVerseCountOk() (*int32, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.VerseCount, true
}

// SetVerseCount sets field value
func (o *Chapter) SetVerseCount(v int32) {
	o.VerseCount = v
}

// GetNext returns the Next field value if set, zero value otherwise.
func (o *Chapter) GetNext() ChapterNext {
	if o == nil || o.Next == nil {
		var ret ChapterNext
		return ret
	}
	return *o.Next
}

// GetNextOk returns a tuple with the Next field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Chapter) GetNextOk() (*ChapterNext, bool) {
	if o == nil || o.Next == nil {
		return nil, false
	}
	return o.Next, true
}

// HasNext returns a boolean if a field has been set.
func (o *Chapter) HasNext() bool {
	if o != nil && o.Next != nil {
		return true
	}

	return false
}

// SetNext gets a reference to the given ChapterNext and assigns it to the Next field.
func (o *Chapter) SetNext(v ChapterNext) {
	o.Next = &v
}

// GetPrevious returns the Previous field value if set, zero value otherwise.
func (o *Chapter) GetPrevious() ChapterNext {
	if o == nil || o.Previous == nil {
		var ret ChapterNext
		return ret
	}
	return *o.Previous
}

// GetPreviousOk returns a tuple with the Previous field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Chapter) GetPreviousOk() (*ChapterNext, bool) {
	if o == nil || o.Previous == nil {
		return nil, false
	}
	return o.Previous, true
}

// HasPrevious returns a boolean if a field has been set.
func (o *Chapter) HasPrevious() bool {
	if o != nil && o.Previous != nil {
		return true
	}

	return false
}

// SetPrevious gets a reference to the given ChapterNext and assigns it to the Previous field.
func (o *Chapter) SetPrevious(v ChapterNext) {
	o.Previous = &v
}

// GetCopyright returns the Copyright field value
func (o *Chapter) GetCopyright() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Copyright
}

// GetCopyrightOk returns a tuple with the Copyright field value
// and a boolean to check if the value has been set.
func (o *Chapter) GetCopyrightOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Copyright, true
}

// SetCopyright sets field value
func (o *Chapter) SetCopyright(v string) {
	o.Copyright = v
}

func (o Chapter) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["id"] = o.Id
	}
	if true {
		toSerialize["bibleId"] = o.BibleId
	}
	if true {
		toSerialize["number"] = o.Number
	}
	if true {
		toSerialize["bookId"] = o.BookId
	}
	if true {
		toSerialize["content"] = o.Content
	}
	if true {
		toSerialize["reference"] = o.Reference
	}
	if true {
		toSerialize["verseCount"] = o.VerseCount
	}
	if o.Next != nil {
		toSerialize["next"] = o.Next
	}
	if o.Previous != nil {
		toSerialize["previous"] = o.Previous
	}
	if true {
		toSerialize["copyright"] = o.Copyright
	}
	return json.Marshal(toSerialize)
}

type NullableChapter struct {
	value *Chapter
	isSet bool
}

func (v NullableChapter) Get() *Chapter {
	return v.value
}

func (v *NullableChapter) Set(val *Chapter) {
	v.value = val
	v.isSet = true
}

func (v NullableChapter) IsSet() bool {
	return v.isSet
}

func (v *NullableChapter) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableChapter(val *Chapter) *NullableChapter {
	return &NullableChapter{value: val, isSet: true}
}

func (v NullableChapter) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableChapter) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


