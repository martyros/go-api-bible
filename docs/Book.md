# Book

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**BibleId** | **string** |  | 
**Abbreviation** | **string** |  | 
**Name** | **string** |  | 
**NameLong** | **string** |  | 
**Chapters** | Pointer to [**[]ChapterSummary**](ChapterSummary.md) |  | [optional] 

## Methods

### NewBook

`func NewBook(id string, bibleId string, abbreviation string, name string, nameLong string, ) *Book`

NewBook instantiates a new Book object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBookWithDefaults

`func NewBookWithDefaults() *Book`

NewBookWithDefaults instantiates a new Book object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Book) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Book) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Book) SetId(v string)`

SetId sets Id field to given value.


### GetBibleId

`func (o *Book) GetBibleId() string`

GetBibleId returns the BibleId field if non-nil, zero value otherwise.

### GetBibleIdOk

`func (o *Book) GetBibleIdOk() (*string, bool)`

GetBibleIdOk returns a tuple with the BibleId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBibleId

`func (o *Book) SetBibleId(v string)`

SetBibleId sets BibleId field to given value.


### GetAbbreviation

`func (o *Book) GetAbbreviation() string`

GetAbbreviation returns the Abbreviation field if non-nil, zero value otherwise.

### GetAbbreviationOk

`func (o *Book) GetAbbreviationOk() (*string, bool)`

GetAbbreviationOk returns a tuple with the Abbreviation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAbbreviation

`func (o *Book) SetAbbreviation(v string)`

SetAbbreviation sets Abbreviation field to given value.


### GetName

`func (o *Book) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Book) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Book) SetName(v string)`

SetName sets Name field to given value.


### GetNameLong

`func (o *Book) GetNameLong() string`

GetNameLong returns the NameLong field if non-nil, zero value otherwise.

### GetNameLongOk

`func (o *Book) GetNameLongOk() (*string, bool)`

GetNameLongOk returns a tuple with the NameLong field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNameLong

`func (o *Book) SetNameLong(v string)`

SetNameLong sets NameLong field to given value.


### GetChapters

`func (o *Book) GetChapters() []ChapterSummary`

GetChapters returns the Chapters field if non-nil, zero value otherwise.

### GetChaptersOk

`func (o *Book) GetChaptersOk() (*[]ChapterSummary, bool)`

GetChaptersOk returns a tuple with the Chapters field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChapters

`func (o *Book) SetChapters(v []ChapterSummary)`

SetChapters sets Chapters field to given value.

### HasChapters

`func (o *Book) HasChapters() bool`

HasChapters returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


