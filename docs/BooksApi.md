# \BooksApi

All URIs are relative to *https://api.scripture.api.bible*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetAudioBook**](BooksApi.md#GetAudioBook) | **Get** /v1/audio-bibles/{audioBibleId}/books/{bookId} | 
[**GetAudioBooks**](BooksApi.md#GetAudioBooks) | **Get** /v1/audio-bibles/{audioBibleId}/books | 
[**GetBook**](BooksApi.md#GetBook) | **Get** /v1/bibles/{bibleId}/books/{bookId} | 
[**GetBooks**](BooksApi.md#GetBooks) | **Get** /v1/bibles/{bibleId}/books | 



## GetAudioBook

> InlineResponse2003 GetAudioBook(ctx, audioBibleId, bookId).IncludeChapters(includeChapters).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    audioBibleId := "audioBibleId_example" // string | Id of audio Bible whose Book to fetch
    bookId := "bookId_example" // string | Id of the Book to fetch
    includeChapters := true // bool | Boolean indicating if an array of chapter summaries should be included in the results. Defaults to false.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BooksApi.GetAudioBook(context.Background(), audioBibleId, bookId).IncludeChapters(includeChapters).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BooksApi.GetAudioBook``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAudioBook`: InlineResponse2003
    fmt.Fprintf(os.Stdout, "Response from `BooksApi.GetAudioBook`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**audioBibleId** | **string** | Id of audio Bible whose Book to fetch | 
**bookId** | **string** | Id of the Book to fetch | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetAudioBookRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **includeChapters** | **bool** | Boolean indicating if an array of chapter summaries should be included in the results. Defaults to false.  | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAudioBooks

> InlineResponse2002 GetAudioBooks(ctx, audioBibleId).IncludeChapters(includeChapters).IncludeChaptersAndSections(includeChaptersAndSections).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    audioBibleId := "audioBibleId_example" // string | Id of audio Bible whose Book to fetch
    includeChapters := true // bool | Boolean indicating if an array of chapter summaries should be included in the results. Defaults to false.  (optional)
    includeChaptersAndSections := true // bool | Boolean indicating if an array of chapter summaries and an array of sections should be included in the results. Defaults to false.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BooksApi.GetAudioBooks(context.Background(), audioBibleId).IncludeChapters(includeChapters).IncludeChaptersAndSections(includeChaptersAndSections).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BooksApi.GetAudioBooks``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAudioBooks`: InlineResponse2002
    fmt.Fprintf(os.Stdout, "Response from `BooksApi.GetAudioBooks`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**audioBibleId** | **string** | Id of audio Bible whose Book to fetch | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetAudioBooksRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **includeChapters** | **bool** | Boolean indicating if an array of chapter summaries should be included in the results. Defaults to false.  | 
 **includeChaptersAndSections** | **bool** | Boolean indicating if an array of chapter summaries and an array of sections should be included in the results. Defaults to false.  | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetBook

> InlineResponse2003 GetBook(ctx, bibleId, bookId).IncludeChapters(includeChapters).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bibleId := "bibleId_example" // string | Id of Bible whose Book to fetch
    bookId := "bookId_example" // string | Id of the Book to fetch
    includeChapters := true // bool | Boolean indicating if an array of chapter summaries should be included in the results. Defaults to false.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BooksApi.GetBook(context.Background(), bibleId, bookId).IncludeChapters(includeChapters).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BooksApi.GetBook``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBook`: InlineResponse2003
    fmt.Fprintf(os.Stdout, "Response from `BooksApi.GetBook`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**bibleId** | **string** | Id of Bible whose Book to fetch | 
**bookId** | **string** | Id of the Book to fetch | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetBookRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **includeChapters** | **bool** | Boolean indicating if an array of chapter summaries should be included in the results. Defaults to false.  | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetBooks

> InlineResponse2002 GetBooks(ctx, bibleId).IncludeChapters(includeChapters).IncludeChaptersAndSections(includeChaptersAndSections).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bibleId := "bibleId_example" // string | Id of Bible whose Book to fetch
    includeChapters := true // bool | Boolean indicating if an array of chapter summaries should be included in the results. Defaults to false.  (optional)
    includeChaptersAndSections := true // bool | Boolean indicating if an array of chapter summaries and an array of sections should be included in the results. Defaults to false.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BooksApi.GetBooks(context.Background(), bibleId).IncludeChapters(includeChapters).IncludeChaptersAndSections(includeChaptersAndSections).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BooksApi.GetBooks``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBooks`: InlineResponse2002
    fmt.Fprintf(os.Stdout, "Response from `BooksApi.GetBooks`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**bibleId** | **string** | Id of Bible whose Book to fetch | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetBooksRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **includeChapters** | **bool** | Boolean indicating if an array of chapter summaries should be included in the results. Defaults to false.  | 
 **includeChaptersAndSections** | **bool** | Boolean indicating if an array of chapter summaries and an array of sections should be included in the results. Defaults to false.  | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

