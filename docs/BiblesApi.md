# \BiblesApi

All URIs are relative to *https://api.scripture.api.bible*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetAudioBible**](BiblesApi.md#GetAudioBible) | **Get** /v1/audio-bibles/{audioBibleId} | 
[**GetAudioBibles**](BiblesApi.md#GetAudioBibles) | **Get** /v1/audio-bibles | 
[**GetBible**](BiblesApi.md#GetBible) | **Get** /v1/bibles/{bibleId} | 
[**GetBibles**](BiblesApi.md#GetBibles) | **Get** /v1/bibles | 



## GetAudioBible

> InlineResponse20012 GetAudioBible(ctx, audioBibleId).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    audioBibleId := "audioBibleId_example" // string | Id of audio Bible to be fetched

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BiblesApi.GetAudioBible(context.Background(), audioBibleId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BiblesApi.GetAudioBible``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAudioBible`: InlineResponse20012
    fmt.Fprintf(os.Stdout, "Response from `BiblesApi.GetAudioBible`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**audioBibleId** | **string** | Id of audio Bible to be fetched | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetAudioBibleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**InlineResponse20012**](InlineResponse20012.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAudioBibles

> InlineResponse200 GetAudioBibles(ctx).Language(language).Abbreviation(abbreviation).Name(name).Ids(ids).BibleId(bibleId).IncludeFullDetails(includeFullDetails).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    language := "language_example" // string | ISO 639-3 three digit language code used to filter results (optional)
    abbreviation := "abbreviation_example" // string | Bible abbreviation to search for (optional)
    name := "name_example" // string | Bible name to search for (optional)
    ids := "ids_example" // string | Comma separated list of Bible Ids to return (optional)
    bibleId := "bibleId_example" // string | bibleId of related text Bible used to filter audio bible results (optional)
    includeFullDetails := true // bool | Boolean to include full Bible details (e.g. copyright and promo info) (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BiblesApi.GetAudioBibles(context.Background()).Language(language).Abbreviation(abbreviation).Name(name).Ids(ids).BibleId(bibleId).IncludeFullDetails(includeFullDetails).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BiblesApi.GetAudioBibles``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAudioBibles`: InlineResponse200
    fmt.Fprintf(os.Stdout, "Response from `BiblesApi.GetAudioBibles`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetAudioBiblesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language** | **string** | ISO 639-3 three digit language code used to filter results | 
 **abbreviation** | **string** | Bible abbreviation to search for | 
 **name** | **string** | Bible name to search for | 
 **ids** | **string** | Comma separated list of Bible Ids to return | 
 **bibleId** | **string** | bibleId of related text Bible used to filter audio bible results | 
 **includeFullDetails** | **bool** | Boolean to include full Bible details (e.g. copyright and promo info) | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetBible

> InlineResponse2001 GetBible(ctx, bibleId).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bibleId := "bibleId_example" // string | Id of Bible to be fetched

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BiblesApi.GetBible(context.Background(), bibleId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BiblesApi.GetBible``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBible`: InlineResponse2001
    fmt.Fprintf(os.Stdout, "Response from `BiblesApi.GetBible`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**bibleId** | **string** | Id of Bible to be fetched | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetBibleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetBibles

> InlineResponse200 GetBibles(ctx).Language(language).Abbreviation(abbreviation).Name(name).Ids(ids).IncludeFullDetails(includeFullDetails).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    language := "language_example" // string | ISO 639-3 three digit language code used to filter results (optional)
    abbreviation := "abbreviation_example" // string | Bible abbreviation to search for (optional)
    name := "name_example" // string | Bible name to search for (optional)
    ids := "ids_example" // string | Comma separated list of Bible Ids to return (optional)
    includeFullDetails := true // bool | Boolean to include full Bible details (e.g. copyright and promo info) (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BiblesApi.GetBibles(context.Background()).Language(language).Abbreviation(abbreviation).Name(name).Ids(ids).IncludeFullDetails(includeFullDetails).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BiblesApi.GetBibles``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBibles`: InlineResponse200
    fmt.Fprintf(os.Stdout, "Response from `BiblesApi.GetBibles`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetBiblesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language** | **string** | ISO 639-3 three digit language code used to filter results | 
 **abbreviation** | **string** | Bible abbreviation to search for | 
 **name** | **string** | Bible name to search for | 
 **ids** | **string** | Comma separated list of Bible Ids to return | 
 **includeFullDetails** | **bool** | Boolean to include full Bible details (e.g. copyright and promo info) | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

