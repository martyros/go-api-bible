# Language

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Name** | **string** |  | 
**NameLocal** | **string** |  | 
**Script** | **string** |  | 
**ScriptDirection** | **string** |  | 

## Methods

### NewLanguage

`func NewLanguage(id string, name string, nameLocal string, script string, scriptDirection string, ) *Language`

NewLanguage instantiates a new Language object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLanguageWithDefaults

`func NewLanguageWithDefaults() *Language`

NewLanguageWithDefaults instantiates a new Language object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Language) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Language) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Language) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *Language) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Language) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Language) SetName(v string)`

SetName sets Name field to given value.


### GetNameLocal

`func (o *Language) GetNameLocal() string`

GetNameLocal returns the NameLocal field if non-nil, zero value otherwise.

### GetNameLocalOk

`func (o *Language) GetNameLocalOk() (*string, bool)`

GetNameLocalOk returns a tuple with the NameLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNameLocal

`func (o *Language) SetNameLocal(v string)`

SetNameLocal sets NameLocal field to given value.


### GetScript

`func (o *Language) GetScript() string`

GetScript returns the Script field if non-nil, zero value otherwise.

### GetScriptOk

`func (o *Language) GetScriptOk() (*string, bool)`

GetScriptOk returns a tuple with the Script field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScript

`func (o *Language) SetScript(v string)`

SetScript sets Script field to given value.


### GetScriptDirection

`func (o *Language) GetScriptDirection() string`

GetScriptDirection returns the ScriptDirection field if non-nil, zero value otherwise.

### GetScriptDirectionOk

`func (o *Language) GetScriptDirectionOk() (*string, bool)`

GetScriptDirectionOk returns a tuple with the ScriptDirection field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScriptDirection

`func (o *Language) SetScriptDirection(v string)`

SetScriptDirection sets ScriptDirection field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


