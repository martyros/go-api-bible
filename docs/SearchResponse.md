# SearchResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Query** | **string** |  | 
**Limit** | **int32** |  | 
**Offset** | **int32** |  | 
**Total** | **int32** |  | 
**VerseCount** | **int32** |  | 
**Verses** | [**[]SearchVerse**](SearchVerse.md) |  | 
**Passages** | Pointer to [**[]Passage**](Passage.md) |  | [optional] 

## Methods

### NewSearchResponse

`func NewSearchResponse(query string, limit int32, offset int32, total int32, verseCount int32, verses []SearchVerse, ) *SearchResponse`

NewSearchResponse instantiates a new SearchResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSearchResponseWithDefaults

`func NewSearchResponseWithDefaults() *SearchResponse`

NewSearchResponseWithDefaults instantiates a new SearchResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetQuery

`func (o *SearchResponse) GetQuery() string`

GetQuery returns the Query field if non-nil, zero value otherwise.

### GetQueryOk

`func (o *SearchResponse) GetQueryOk() (*string, bool)`

GetQueryOk returns a tuple with the Query field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQuery

`func (o *SearchResponse) SetQuery(v string)`

SetQuery sets Query field to given value.


### GetLimit

`func (o *SearchResponse) GetLimit() int32`

GetLimit returns the Limit field if non-nil, zero value otherwise.

### GetLimitOk

`func (o *SearchResponse) GetLimitOk() (*int32, bool)`

GetLimitOk returns a tuple with the Limit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLimit

`func (o *SearchResponse) SetLimit(v int32)`

SetLimit sets Limit field to given value.


### GetOffset

`func (o *SearchResponse) GetOffset() int32`

GetOffset returns the Offset field if non-nil, zero value otherwise.

### GetOffsetOk

`func (o *SearchResponse) GetOffsetOk() (*int32, bool)`

GetOffsetOk returns a tuple with the Offset field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOffset

`func (o *SearchResponse) SetOffset(v int32)`

SetOffset sets Offset field to given value.


### GetTotal

`func (o *SearchResponse) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *SearchResponse) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *SearchResponse) SetTotal(v int32)`

SetTotal sets Total field to given value.


### GetVerseCount

`func (o *SearchResponse) GetVerseCount() int32`

GetVerseCount returns the VerseCount field if non-nil, zero value otherwise.

### GetVerseCountOk

`func (o *SearchResponse) GetVerseCountOk() (*int32, bool)`

GetVerseCountOk returns a tuple with the VerseCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVerseCount

`func (o *SearchResponse) SetVerseCount(v int32)`

SetVerseCount sets VerseCount field to given value.


### GetVerses

`func (o *SearchResponse) GetVerses() []SearchVerse`

GetVerses returns the Verses field if non-nil, zero value otherwise.

### GetVersesOk

`func (o *SearchResponse) GetVersesOk() (*[]SearchVerse, bool)`

GetVersesOk returns a tuple with the Verses field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVerses

`func (o *SearchResponse) SetVerses(v []SearchVerse)`

SetVerses sets Verses field to given value.


### GetPassages

`func (o *SearchResponse) GetPassages() []Passage`

GetPassages returns the Passages field if non-nil, zero value otherwise.

### GetPassagesOk

`func (o *SearchResponse) GetPassagesOk() (*[]Passage, bool)`

GetPassagesOk returns a tuple with the Passages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPassages

`func (o *SearchResponse) SetPassages(v []Passage)`

SetPassages sets Passages field to given value.

### HasPassages

`func (o *SearchResponse) HasPassages() bool`

HasPassages returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


