# Bible

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**DblId** | **string** |  | 
**Abbreviation** | **string** |  | 
**AbbreviationLocal** | **string** |  | 
**Copyright** | **string** |  | 
**Language** | [**Language**](Language.md) |  | 
**Countries** | [**[]BibleSummaryCountries**](BibleSummaryCountries.md) |  | 
**Name** | **string** |  | 
**NameLocal** | **string** |  | 
**Description** | **string** |  | 
**DescriptionLocal** | **string** |  | 
**Info** | **string** |  | 
**Type** | **string** |  | 
**UpdatedAt** | **time.Time** |  | 
**RelatedDbl** | **string** |  | 
**AudioBibles** | [**[]AudioBibleSummary**](AudioBibleSummary.md) |  | 

## Methods

### NewBible

`func NewBible(id string, dblId string, abbreviation string, abbreviationLocal string, copyright string, language Language, countries []BibleSummaryCountries, name string, nameLocal string, description string, descriptionLocal string, info string, type_ string, updatedAt time.Time, relatedDbl string, audioBibles []AudioBibleSummary, ) *Bible`

NewBible instantiates a new Bible object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBibleWithDefaults

`func NewBibleWithDefaults() *Bible`

NewBibleWithDefaults instantiates a new Bible object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Bible) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Bible) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Bible) SetId(v string)`

SetId sets Id field to given value.


### GetDblId

`func (o *Bible) GetDblId() string`

GetDblId returns the DblId field if non-nil, zero value otherwise.

### GetDblIdOk

`func (o *Bible) GetDblIdOk() (*string, bool)`

GetDblIdOk returns a tuple with the DblId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDblId

`func (o *Bible) SetDblId(v string)`

SetDblId sets DblId field to given value.


### GetAbbreviation

`func (o *Bible) GetAbbreviation() string`

GetAbbreviation returns the Abbreviation field if non-nil, zero value otherwise.

### GetAbbreviationOk

`func (o *Bible) GetAbbreviationOk() (*string, bool)`

GetAbbreviationOk returns a tuple with the Abbreviation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAbbreviation

`func (o *Bible) SetAbbreviation(v string)`

SetAbbreviation sets Abbreviation field to given value.


### GetAbbreviationLocal

`func (o *Bible) GetAbbreviationLocal() string`

GetAbbreviationLocal returns the AbbreviationLocal field if non-nil, zero value otherwise.

### GetAbbreviationLocalOk

`func (o *Bible) GetAbbreviationLocalOk() (*string, bool)`

GetAbbreviationLocalOk returns a tuple with the AbbreviationLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAbbreviationLocal

`func (o *Bible) SetAbbreviationLocal(v string)`

SetAbbreviationLocal sets AbbreviationLocal field to given value.


### GetCopyright

`func (o *Bible) GetCopyright() string`

GetCopyright returns the Copyright field if non-nil, zero value otherwise.

### GetCopyrightOk

`func (o *Bible) GetCopyrightOk() (*string, bool)`

GetCopyrightOk returns a tuple with the Copyright field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCopyright

`func (o *Bible) SetCopyright(v string)`

SetCopyright sets Copyright field to given value.


### GetLanguage

`func (o *Bible) GetLanguage() Language`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *Bible) GetLanguageOk() (*Language, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *Bible) SetLanguage(v Language)`

SetLanguage sets Language field to given value.


### GetCountries

`func (o *Bible) GetCountries() []BibleSummaryCountries`

GetCountries returns the Countries field if non-nil, zero value otherwise.

### GetCountriesOk

`func (o *Bible) GetCountriesOk() (*[]BibleSummaryCountries, bool)`

GetCountriesOk returns a tuple with the Countries field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountries

`func (o *Bible) SetCountries(v []BibleSummaryCountries)`

SetCountries sets Countries field to given value.


### GetName

`func (o *Bible) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Bible) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Bible) SetName(v string)`

SetName sets Name field to given value.


### GetNameLocal

`func (o *Bible) GetNameLocal() string`

GetNameLocal returns the NameLocal field if non-nil, zero value otherwise.

### GetNameLocalOk

`func (o *Bible) GetNameLocalOk() (*string, bool)`

GetNameLocalOk returns a tuple with the NameLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNameLocal

`func (o *Bible) SetNameLocal(v string)`

SetNameLocal sets NameLocal field to given value.


### GetDescription

`func (o *Bible) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *Bible) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *Bible) SetDescription(v string)`

SetDescription sets Description field to given value.


### GetDescriptionLocal

`func (o *Bible) GetDescriptionLocal() string`

GetDescriptionLocal returns the DescriptionLocal field if non-nil, zero value otherwise.

### GetDescriptionLocalOk

`func (o *Bible) GetDescriptionLocalOk() (*string, bool)`

GetDescriptionLocalOk returns a tuple with the DescriptionLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescriptionLocal

`func (o *Bible) SetDescriptionLocal(v string)`

SetDescriptionLocal sets DescriptionLocal field to given value.


### GetInfo

`func (o *Bible) GetInfo() string`

GetInfo returns the Info field if non-nil, zero value otherwise.

### GetInfoOk

`func (o *Bible) GetInfoOk() (*string, bool)`

GetInfoOk returns a tuple with the Info field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInfo

`func (o *Bible) SetInfo(v string)`

SetInfo sets Info field to given value.


### GetType

`func (o *Bible) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Bible) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Bible) SetType(v string)`

SetType sets Type field to given value.


### GetUpdatedAt

`func (o *Bible) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Bible) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Bible) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetRelatedDbl

`func (o *Bible) GetRelatedDbl() string`

GetRelatedDbl returns the RelatedDbl field if non-nil, zero value otherwise.

### GetRelatedDblOk

`func (o *Bible) GetRelatedDblOk() (*string, bool)`

GetRelatedDblOk returns a tuple with the RelatedDbl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRelatedDbl

`func (o *Bible) SetRelatedDbl(v string)`

SetRelatedDbl sets RelatedDbl field to given value.


### GetAudioBibles

`func (o *Bible) GetAudioBibles() []AudioBibleSummary`

GetAudioBibles returns the AudioBibles field if non-nil, zero value otherwise.

### GetAudioBiblesOk

`func (o *Bible) GetAudioBiblesOk() (*[]AudioBibleSummary, bool)`

GetAudioBiblesOk returns a tuple with the AudioBibles field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAudioBibles

`func (o *Bible) SetAudioBibles(v []AudioBibleSummary)`

SetAudioBibles sets AudioBibles field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


