# Section

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**BibleId** | **string** |  | 
**BookId** | **string** |  | 
**ChapterId** | **string** |  | 
**Title** | **string** |  | 
**Content** | **string** |  | 
**VerseCount** | **int32** |  | 
**FirstVerseId** | **string** |  | 
**LastVerseId** | **string** |  | 
**FirstVerseOrgId** | **string** |  | 
**LastVerseOrgId** | **string** |  | 
**Copyright** | **string** |  | 
**Next** | Pointer to [**SectionNext**](SectionNext.md) |  | [optional] 
**Previous** | Pointer to [**SectionNext**](SectionNext.md) |  | [optional] 

## Methods

### NewSection

`func NewSection(id string, bibleId string, bookId string, chapterId string, title string, content string, verseCount int32, firstVerseId string, lastVerseId string, firstVerseOrgId string, lastVerseOrgId string, copyright string, ) *Section`

NewSection instantiates a new Section object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSectionWithDefaults

`func NewSectionWithDefaults() *Section`

NewSectionWithDefaults instantiates a new Section object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Section) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Section) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Section) SetId(v string)`

SetId sets Id field to given value.


### GetBibleId

`func (o *Section) GetBibleId() string`

GetBibleId returns the BibleId field if non-nil, zero value otherwise.

### GetBibleIdOk

`func (o *Section) GetBibleIdOk() (*string, bool)`

GetBibleIdOk returns a tuple with the BibleId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBibleId

`func (o *Section) SetBibleId(v string)`

SetBibleId sets BibleId field to given value.


### GetBookId

`func (o *Section) GetBookId() string`

GetBookId returns the BookId field if non-nil, zero value otherwise.

### GetBookIdOk

`func (o *Section) GetBookIdOk() (*string, bool)`

GetBookIdOk returns a tuple with the BookId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBookId

`func (o *Section) SetBookId(v string)`

SetBookId sets BookId field to given value.


### GetChapterId

`func (o *Section) GetChapterId() string`

GetChapterId returns the ChapterId field if non-nil, zero value otherwise.

### GetChapterIdOk

`func (o *Section) GetChapterIdOk() (*string, bool)`

GetChapterIdOk returns a tuple with the ChapterId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChapterId

`func (o *Section) SetChapterId(v string)`

SetChapterId sets ChapterId field to given value.


### GetTitle

`func (o *Section) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *Section) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *Section) SetTitle(v string)`

SetTitle sets Title field to given value.


### GetContent

`func (o *Section) GetContent() string`

GetContent returns the Content field if non-nil, zero value otherwise.

### GetContentOk

`func (o *Section) GetContentOk() (*string, bool)`

GetContentOk returns a tuple with the Content field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContent

`func (o *Section) SetContent(v string)`

SetContent sets Content field to given value.


### GetVerseCount

`func (o *Section) GetVerseCount() int32`

GetVerseCount returns the VerseCount field if non-nil, zero value otherwise.

### GetVerseCountOk

`func (o *Section) GetVerseCountOk() (*int32, bool)`

GetVerseCountOk returns a tuple with the VerseCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVerseCount

`func (o *Section) SetVerseCount(v int32)`

SetVerseCount sets VerseCount field to given value.


### GetFirstVerseId

`func (o *Section) GetFirstVerseId() string`

GetFirstVerseId returns the FirstVerseId field if non-nil, zero value otherwise.

### GetFirstVerseIdOk

`func (o *Section) GetFirstVerseIdOk() (*string, bool)`

GetFirstVerseIdOk returns a tuple with the FirstVerseId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstVerseId

`func (o *Section) SetFirstVerseId(v string)`

SetFirstVerseId sets FirstVerseId field to given value.


### GetLastVerseId

`func (o *Section) GetLastVerseId() string`

GetLastVerseId returns the LastVerseId field if non-nil, zero value otherwise.

### GetLastVerseIdOk

`func (o *Section) GetLastVerseIdOk() (*string, bool)`

GetLastVerseIdOk returns a tuple with the LastVerseId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastVerseId

`func (o *Section) SetLastVerseId(v string)`

SetLastVerseId sets LastVerseId field to given value.


### GetFirstVerseOrgId

`func (o *Section) GetFirstVerseOrgId() string`

GetFirstVerseOrgId returns the FirstVerseOrgId field if non-nil, zero value otherwise.

### GetFirstVerseOrgIdOk

`func (o *Section) GetFirstVerseOrgIdOk() (*string, bool)`

GetFirstVerseOrgIdOk returns a tuple with the FirstVerseOrgId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstVerseOrgId

`func (o *Section) SetFirstVerseOrgId(v string)`

SetFirstVerseOrgId sets FirstVerseOrgId field to given value.


### GetLastVerseOrgId

`func (o *Section) GetLastVerseOrgId() string`

GetLastVerseOrgId returns the LastVerseOrgId field if non-nil, zero value otherwise.

### GetLastVerseOrgIdOk

`func (o *Section) GetLastVerseOrgIdOk() (*string, bool)`

GetLastVerseOrgIdOk returns a tuple with the LastVerseOrgId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastVerseOrgId

`func (o *Section) SetLastVerseOrgId(v string)`

SetLastVerseOrgId sets LastVerseOrgId field to given value.


### GetCopyright

`func (o *Section) GetCopyright() string`

GetCopyright returns the Copyright field if non-nil, zero value otherwise.

### GetCopyrightOk

`func (o *Section) GetCopyrightOk() (*string, bool)`

GetCopyrightOk returns a tuple with the Copyright field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCopyright

`func (o *Section) SetCopyright(v string)`

SetCopyright sets Copyright field to given value.


### GetNext

`func (o *Section) GetNext() SectionNext`

GetNext returns the Next field if non-nil, zero value otherwise.

### GetNextOk

`func (o *Section) GetNextOk() (*SectionNext, bool)`

GetNextOk returns a tuple with the Next field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNext

`func (o *Section) SetNext(v SectionNext)`

SetNext sets Next field to given value.

### HasNext

`func (o *Section) HasNext() bool`

HasNext returns a boolean if a field has been set.

### GetPrevious

`func (o *Section) GetPrevious() SectionNext`

GetPrevious returns the Previous field if non-nil, zero value otherwise.

### GetPreviousOk

`func (o *Section) GetPreviousOk() (*SectionNext, bool)`

GetPreviousOk returns a tuple with the Previous field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrevious

`func (o *Section) SetPrevious(v SectionNext)`

SetPrevious sets Previous field to given value.

### HasPrevious

`func (o *Section) HasPrevious() bool`

HasPrevious returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


