# \ChaptersApi

All URIs are relative to *https://api.scripture.api.bible*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetAudioChapter**](ChaptersApi.md#GetAudioChapter) | **Get** /v1/audio-bibles/{audioBibleId}/chapters/{chapterId} | 
[**GetAudioChapters**](ChaptersApi.md#GetAudioChapters) | **Get** /v1/audio-bibles/{audioBibleId}/books/{bookId}/chapters | 
[**GetChapter**](ChaptersApi.md#GetChapter) | **Get** /v1/bibles/{bibleId}/chapters/{chapterId} | 
[**GetChapters**](ChaptersApi.md#GetChapters) | **Get** /v1/bibles/{bibleId}/books/{bookId}/chapters | 



## GetAudioChapter

> InlineResponse20013 GetAudioChapter(ctx, audioBibleId, chapterId).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    audioBibleId := "audioBibleId_example" // string | Id of Bible whose Chapter to fetch
    chapterId := "chapterId_example" // string | Id of the Chapter to fetch

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ChaptersApi.GetAudioChapter(context.Background(), audioBibleId, chapterId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ChaptersApi.GetAudioChapter``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAudioChapter`: InlineResponse20013
    fmt.Fprintf(os.Stdout, "Response from `ChaptersApi.GetAudioChapter`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**audioBibleId** | **string** | Id of Bible whose Chapter to fetch | 
**chapterId** | **string** | Id of the Chapter to fetch | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetAudioChapterRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**InlineResponse20013**](InlineResponse20013.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAudioChapters

> InlineResponse2004 GetAudioChapters(ctx, audioBibleId, bookId).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    audioBibleId := "audioBibleId_example" // string | Id of Bible whose Chapters to fetch
    bookId := "bookId_example" // string | Id of the Book whose Chapters to fetch

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ChaptersApi.GetAudioChapters(context.Background(), audioBibleId, bookId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ChaptersApi.GetAudioChapters``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAudioChapters`: InlineResponse2004
    fmt.Fprintf(os.Stdout, "Response from `ChaptersApi.GetAudioChapters`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**audioBibleId** | **string** | Id of Bible whose Chapters to fetch | 
**bookId** | **string** | Id of the Book whose Chapters to fetch | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetAudioChaptersRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetChapter

> InlineResponse2005 GetChapter(ctx, bibleId, chapterId).ContentType(contentType).IncludeNotes(includeNotes).IncludeTitles(includeTitles).IncludeChapterNumbers(includeChapterNumbers).IncludeVerseNumbers(includeVerseNumbers).IncludeVerseSpans(includeVerseSpans).Parallels(parallels).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bibleId := "bibleId_example" // string | Id of Bible whose Chapter to fetch
    chapterId := "chapterId_example" // string | Id of the Chapter to fetch
    contentType := "contentType_example" // string | Content type to be returned in the content property.  Supported values are `html` (default), `json` (beta), and `text` (beta) (optional) (default to "html")
    includeNotes := true // bool | Include footnotes in content (optional) (default to false)
    includeTitles := true // bool | Include section titles in content (optional) (default to true)
    includeChapterNumbers := true // bool | Include chapter numbers in content (optional) (default to false)
    includeVerseNumbers := true // bool | Include verse numbers in content. (optional) (default to true)
    includeVerseSpans := true // bool | Include spans that wrap verse numbers and verse text for bible content. (optional) (default to false)
    parallels := "parallels_example" // string | Comma delimited list of bibleIds to include (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ChaptersApi.GetChapter(context.Background(), bibleId, chapterId).ContentType(contentType).IncludeNotes(includeNotes).IncludeTitles(includeTitles).IncludeChapterNumbers(includeChapterNumbers).IncludeVerseNumbers(includeVerseNumbers).IncludeVerseSpans(includeVerseSpans).Parallels(parallels).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ChaptersApi.GetChapter``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChapter`: InlineResponse2005
    fmt.Fprintf(os.Stdout, "Response from `ChaptersApi.GetChapter`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**bibleId** | **string** | Id of Bible whose Chapter to fetch | 
**chapterId** | **string** | Id of the Chapter to fetch | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChapterRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **contentType** | **string** | Content type to be returned in the content property.  Supported values are &#x60;html&#x60; (default), &#x60;json&#x60; (beta), and &#x60;text&#x60; (beta) | [default to &quot;html&quot;]
 **includeNotes** | **bool** | Include footnotes in content | [default to false]
 **includeTitles** | **bool** | Include section titles in content | [default to true]
 **includeChapterNumbers** | **bool** | Include chapter numbers in content | [default to false]
 **includeVerseNumbers** | **bool** | Include verse numbers in content. | [default to true]
 **includeVerseSpans** | **bool** | Include spans that wrap verse numbers and verse text for bible content. | [default to false]
 **parallels** | **string** | Comma delimited list of bibleIds to include | 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetChapters

> InlineResponse2004 GetChapters(ctx, bibleId, bookId).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bibleId := "bibleId_example" // string | Id of Bible whose Chapters to fetch
    bookId := "bookId_example" // string | Id of the Book whose Chapters to fetch

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ChaptersApi.GetChapters(context.Background(), bibleId, bookId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ChaptersApi.GetChapters``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChapters`: InlineResponse2004
    fmt.Fprintf(os.Stdout, "Response from `ChaptersApi.GetChapters`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**bibleId** | **string** | Id of Bible whose Chapters to fetch | 
**bookId** | **string** | Id of the Book whose Chapters to fetch | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChaptersRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

