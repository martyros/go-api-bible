# BibleSummaryCountries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Name** | **string** |  | 
**NameLocal** | **string** |  | 

## Methods

### NewBibleSummaryCountries

`func NewBibleSummaryCountries(id string, name string, nameLocal string, ) *BibleSummaryCountries`

NewBibleSummaryCountries instantiates a new BibleSummaryCountries object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBibleSummaryCountriesWithDefaults

`func NewBibleSummaryCountriesWithDefaults() *BibleSummaryCountries`

NewBibleSummaryCountriesWithDefaults instantiates a new BibleSummaryCountries object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *BibleSummaryCountries) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *BibleSummaryCountries) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *BibleSummaryCountries) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *BibleSummaryCountries) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *BibleSummaryCountries) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *BibleSummaryCountries) SetName(v string)`

SetName sets Name field to given value.


### GetNameLocal

`func (o *BibleSummaryCountries) GetNameLocal() string`

GetNameLocal returns the NameLocal field if non-nil, zero value otherwise.

### GetNameLocalOk

`func (o *BibleSummaryCountries) GetNameLocalOk() (*string, bool)`

GetNameLocalOk returns a tuple with the NameLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNameLocal

`func (o *BibleSummaryCountries) SetNameLocal(v string)`

SetNameLocal sets NameLocal field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


