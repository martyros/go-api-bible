# AudioChapterTimecodes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**End** | **string** |  | 
**Start** | **string** |  | 
**VerseId** | **string** |  | 

## Methods

### NewAudioChapterTimecodes

`func NewAudioChapterTimecodes(end string, start string, verseId string, ) *AudioChapterTimecodes`

NewAudioChapterTimecodes instantiates a new AudioChapterTimecodes object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAudioChapterTimecodesWithDefaults

`func NewAudioChapterTimecodesWithDefaults() *AudioChapterTimecodes`

NewAudioChapterTimecodesWithDefaults instantiates a new AudioChapterTimecodes object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetEnd

`func (o *AudioChapterTimecodes) GetEnd() string`

GetEnd returns the End field if non-nil, zero value otherwise.

### GetEndOk

`func (o *AudioChapterTimecodes) GetEndOk() (*string, bool)`

GetEndOk returns a tuple with the End field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEnd

`func (o *AudioChapterTimecodes) SetEnd(v string)`

SetEnd sets End field to given value.


### GetStart

`func (o *AudioChapterTimecodes) GetStart() string`

GetStart returns the Start field if non-nil, zero value otherwise.

### GetStartOk

`func (o *AudioChapterTimecodes) GetStartOk() (*string, bool)`

GetStartOk returns a tuple with the Start field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStart

`func (o *AudioChapterTimecodes) SetStart(v string)`

SetStart sets Start field to given value.


### GetVerseId

`func (o *AudioChapterTimecodes) GetVerseId() string`

GetVerseId returns the VerseId field if non-nil, zero value otherwise.

### GetVerseIdOk

`func (o *AudioChapterTimecodes) GetVerseIdOk() (*string, bool)`

GetVerseIdOk returns a tuple with the VerseId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVerseId

`func (o *AudioChapterTimecodes) SetVerseId(v string)`

SetVerseId sets VerseId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


