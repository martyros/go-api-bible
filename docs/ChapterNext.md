# ChapterNext

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**BookId** | Pointer to **string** |  | [optional] 
**Number** | Pointer to **string** |  | [optional] 

## Methods

### NewChapterNext

`func NewChapterNext() *ChapterNext`

NewChapterNext instantiates a new ChapterNext object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChapterNextWithDefaults

`func NewChapterNextWithDefaults() *ChapterNext`

NewChapterNextWithDefaults instantiates a new ChapterNext object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ChapterNext) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ChapterNext) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ChapterNext) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *ChapterNext) HasId() bool`

HasId returns a boolean if a field has been set.

### GetBookId

`func (o *ChapterNext) GetBookId() string`

GetBookId returns the BookId field if non-nil, zero value otherwise.

### GetBookIdOk

`func (o *ChapterNext) GetBookIdOk() (*string, bool)`

GetBookIdOk returns a tuple with the BookId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBookId

`func (o *ChapterNext) SetBookId(v string)`

SetBookId sets BookId field to given value.

### HasBookId

`func (o *ChapterNext) HasBookId() bool`

HasBookId returns a boolean if a field has been set.

### GetNumber

`func (o *ChapterNext) GetNumber() string`

GetNumber returns the Number field if non-nil, zero value otherwise.

### GetNumberOk

`func (o *ChapterNext) GetNumberOk() (*string, bool)`

GetNumberOk returns a tuple with the Number field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNumber

`func (o *ChapterNext) SetNumber(v string)`

SetNumber sets Number field to given value.

### HasNumber

`func (o *ChapterNext) HasNumber() bool`

HasNumber returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


