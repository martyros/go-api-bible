# \SectionsApi

All URIs are relative to *https://api.scripture.api.bible*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetBookSections**](SectionsApi.md#GetBookSections) | **Get** /v1/bibles/{bibleId}/books/{bookId}/sections | 
[**GetChapterSections**](SectionsApi.md#GetChapterSections) | **Get** /v1/bibles/{bibleId}/chapters/{chapterId}/sections | 
[**GetSection**](SectionsApi.md#GetSection) | **Get** /v1/bibles/{bibleId}/sections/{sectionId} | 



## GetBookSections

> InlineResponse2007 GetBookSections(ctx, bibleId, bookId).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bibleId := "bibleId_example" // string | Id of Bible whose Sections to fetch
    bookId := "bookId_example" // string | Id of the Book whose Sections to fetch

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.SectionsApi.GetBookSections(context.Background(), bibleId, bookId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `SectionsApi.GetBookSections``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBookSections`: InlineResponse2007
    fmt.Fprintf(os.Stdout, "Response from `SectionsApi.GetBookSections`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**bibleId** | **string** | Id of Bible whose Sections to fetch | 
**bookId** | **string** | Id of the Book whose Sections to fetch | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetBookSectionsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetChapterSections

> InlineResponse2007 GetChapterSections(ctx, bibleId, chapterId).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bibleId := "bibleId_example" // string | Id of Bible whose Sections to fetch
    chapterId := "chapterId_example" // string | Id of the Chapter whose Sections to fetch

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.SectionsApi.GetChapterSections(context.Background(), bibleId, chapterId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `SectionsApi.GetChapterSections``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChapterSections`: InlineResponse2007
    fmt.Fprintf(os.Stdout, "Response from `SectionsApi.GetChapterSections`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**bibleId** | **string** | Id of Bible whose Sections to fetch | 
**chapterId** | **string** | Id of the Chapter whose Sections to fetch | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChapterSectionsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetSection

> InlineResponse2008 GetSection(ctx, bibleId, sectionId).ContentType(contentType).IncludeNotes(includeNotes).IncludeTitles(includeTitles).IncludeChapterNumbers(includeChapterNumbers).IncludeVerseNumbers(includeVerseNumbers).IncludeVerseSpans(includeVerseSpans).Parallels(parallels).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bibleId := "bibleId_example" // string | Id of Bible whose Section to fetch
    sectionId := "sectionId_example" // string | Id of the Section to fetch
    contentType := "contentType_example" // string | Content type to be returned in the content property.  Supported values are `html` (default), `json` (beta), and `text` (beta) (optional) (default to "html")
    includeNotes := true // bool | Include footnotes in content (optional) (default to false)
    includeTitles := true // bool | Include section titles in content (optional) (default to true)
    includeChapterNumbers := true // bool | Include chapter numbers in content (optional) (default to false)
    includeVerseNumbers := true // bool | Include verse numbers in content. (optional) (default to true)
    includeVerseSpans := true // bool | Include spans that wrap verse numbers and verse text for bible content. (optional) (default to false)
    parallels := "parallels_example" // string | Comma delimited list of bibleIds to include (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.SectionsApi.GetSection(context.Background(), bibleId, sectionId).ContentType(contentType).IncludeNotes(includeNotes).IncludeTitles(includeTitles).IncludeChapterNumbers(includeChapterNumbers).IncludeVerseNumbers(includeVerseNumbers).IncludeVerseSpans(includeVerseSpans).Parallels(parallels).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `SectionsApi.GetSection``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetSection`: InlineResponse2008
    fmt.Fprintf(os.Stdout, "Response from `SectionsApi.GetSection`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**bibleId** | **string** | Id of Bible whose Section to fetch | 
**sectionId** | **string** | Id of the Section to fetch | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetSectionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **contentType** | **string** | Content type to be returned in the content property.  Supported values are &#x60;html&#x60; (default), &#x60;json&#x60; (beta), and &#x60;text&#x60; (beta) | [default to &quot;html&quot;]
 **includeNotes** | **bool** | Include footnotes in content | [default to false]
 **includeTitles** | **bool** | Include section titles in content | [default to true]
 **includeChapterNumbers** | **bool** | Include chapter numbers in content | [default to false]
 **includeVerseNumbers** | **bool** | Include verse numbers in content. | [default to true]
 **includeVerseSpans** | **bool** | Include spans that wrap verse numbers and verse text for bible content. | [default to false]
 **parallels** | **string** | Comma delimited list of bibleIds to include | 

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

