# SectionSummary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**BibleId** | **string** |  | 
**BookId** | **string** |  | 
**Title** | **string** |  | 
**FirstVerseId** | **string** |  | 
**LastVerseId** | **string** |  | 
**FirstVerseOrgId** | **string** |  | 
**LastVerseOrgId** | **string** |  | 

## Methods

### NewSectionSummary

`func NewSectionSummary(id string, bibleId string, bookId string, title string, firstVerseId string, lastVerseId string, firstVerseOrgId string, lastVerseOrgId string, ) *SectionSummary`

NewSectionSummary instantiates a new SectionSummary object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSectionSummaryWithDefaults

`func NewSectionSummaryWithDefaults() *SectionSummary`

NewSectionSummaryWithDefaults instantiates a new SectionSummary object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *SectionSummary) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *SectionSummary) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *SectionSummary) SetId(v string)`

SetId sets Id field to given value.


### GetBibleId

`func (o *SectionSummary) GetBibleId() string`

GetBibleId returns the BibleId field if non-nil, zero value otherwise.

### GetBibleIdOk

`func (o *SectionSummary) GetBibleIdOk() (*string, bool)`

GetBibleIdOk returns a tuple with the BibleId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBibleId

`func (o *SectionSummary) SetBibleId(v string)`

SetBibleId sets BibleId field to given value.


### GetBookId

`func (o *SectionSummary) GetBookId() string`

GetBookId returns the BookId field if non-nil, zero value otherwise.

### GetBookIdOk

`func (o *SectionSummary) GetBookIdOk() (*string, bool)`

GetBookIdOk returns a tuple with the BookId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBookId

`func (o *SectionSummary) SetBookId(v string)`

SetBookId sets BookId field to given value.


### GetTitle

`func (o *SectionSummary) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *SectionSummary) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *SectionSummary) SetTitle(v string)`

SetTitle sets Title field to given value.


### GetFirstVerseId

`func (o *SectionSummary) GetFirstVerseId() string`

GetFirstVerseId returns the FirstVerseId field if non-nil, zero value otherwise.

### GetFirstVerseIdOk

`func (o *SectionSummary) GetFirstVerseIdOk() (*string, bool)`

GetFirstVerseIdOk returns a tuple with the FirstVerseId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstVerseId

`func (o *SectionSummary) SetFirstVerseId(v string)`

SetFirstVerseId sets FirstVerseId field to given value.


### GetLastVerseId

`func (o *SectionSummary) GetLastVerseId() string`

GetLastVerseId returns the LastVerseId field if non-nil, zero value otherwise.

### GetLastVerseIdOk

`func (o *SectionSummary) GetLastVerseIdOk() (*string, bool)`

GetLastVerseIdOk returns a tuple with the LastVerseId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastVerseId

`func (o *SectionSummary) SetLastVerseId(v string)`

SetLastVerseId sets LastVerseId field to given value.


### GetFirstVerseOrgId

`func (o *SectionSummary) GetFirstVerseOrgId() string`

GetFirstVerseOrgId returns the FirstVerseOrgId field if non-nil, zero value otherwise.

### GetFirstVerseOrgIdOk

`func (o *SectionSummary) GetFirstVerseOrgIdOk() (*string, bool)`

GetFirstVerseOrgIdOk returns a tuple with the FirstVerseOrgId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstVerseOrgId

`func (o *SectionSummary) SetFirstVerseOrgId(v string)`

SetFirstVerseOrgId sets FirstVerseOrgId field to given value.


### GetLastVerseOrgId

`func (o *SectionSummary) GetLastVerseOrgId() string`

GetLastVerseOrgId returns the LastVerseOrgId field if non-nil, zero value otherwise.

### GetLastVerseOrgIdOk

`func (o *SectionSummary) GetLastVerseOrgIdOk() (*string, bool)`

GetLastVerseOrgIdOk returns a tuple with the LastVerseOrgId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastVerseOrgId

`func (o *SectionSummary) SetLastVerseOrgId(v string)`

SetLastVerseOrgId sets LastVerseOrgId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


