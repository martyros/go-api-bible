# ChapterSummary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**BibleId** | **string** |  | 
**Number** | **string** |  | 
**BookId** | **string** |  | 
**Reference** | **string** |  | 

## Methods

### NewChapterSummary

`func NewChapterSummary(id string, bibleId string, number string, bookId string, reference string, ) *ChapterSummary`

NewChapterSummary instantiates a new ChapterSummary object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChapterSummaryWithDefaults

`func NewChapterSummaryWithDefaults() *ChapterSummary`

NewChapterSummaryWithDefaults instantiates a new ChapterSummary object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ChapterSummary) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ChapterSummary) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ChapterSummary) SetId(v string)`

SetId sets Id field to given value.


### GetBibleId

`func (o *ChapterSummary) GetBibleId() string`

GetBibleId returns the BibleId field if non-nil, zero value otherwise.

### GetBibleIdOk

`func (o *ChapterSummary) GetBibleIdOk() (*string, bool)`

GetBibleIdOk returns a tuple with the BibleId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBibleId

`func (o *ChapterSummary) SetBibleId(v string)`

SetBibleId sets BibleId field to given value.


### GetNumber

`func (o *ChapterSummary) GetNumber() string`

GetNumber returns the Number field if non-nil, zero value otherwise.

### GetNumberOk

`func (o *ChapterSummary) GetNumberOk() (*string, bool)`

GetNumberOk returns a tuple with the Number field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNumber

`func (o *ChapterSummary) SetNumber(v string)`

SetNumber sets Number field to given value.


### GetBookId

`func (o *ChapterSummary) GetBookId() string`

GetBookId returns the BookId field if non-nil, zero value otherwise.

### GetBookIdOk

`func (o *ChapterSummary) GetBookIdOk() (*string, bool)`

GetBookIdOk returns a tuple with the BookId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBookId

`func (o *ChapterSummary) SetBookId(v string)`

SetBookId sets BookId field to given value.


### GetReference

`func (o *ChapterSummary) GetReference() string`

GetReference returns the Reference field if non-nil, zero value otherwise.

### GetReferenceOk

`func (o *ChapterSummary) GetReferenceOk() (*string, bool)`

GetReferenceOk returns a tuple with the Reference field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReference

`func (o *ChapterSummary) SetReference(v string)`

SetReference sets Reference field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


