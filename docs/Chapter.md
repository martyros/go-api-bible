# Chapter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**BibleId** | **string** |  | 
**Number** | **string** |  | 
**BookId** | **string** |  | 
**Content** | **string** |  | 
**Reference** | **string** |  | 
**VerseCount** | **int32** |  | 
**Next** | Pointer to [**ChapterNext**](ChapterNext.md) |  | [optional] 
**Previous** | Pointer to [**ChapterNext**](ChapterNext.md) |  | [optional] 
**Copyright** | **string** |  | 

## Methods

### NewChapter

`func NewChapter(id string, bibleId string, number string, bookId string, content string, reference string, verseCount int32, copyright string, ) *Chapter`

NewChapter instantiates a new Chapter object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChapterWithDefaults

`func NewChapterWithDefaults() *Chapter`

NewChapterWithDefaults instantiates a new Chapter object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Chapter) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Chapter) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Chapter) SetId(v string)`

SetId sets Id field to given value.


### GetBibleId

`func (o *Chapter) GetBibleId() string`

GetBibleId returns the BibleId field if non-nil, zero value otherwise.

### GetBibleIdOk

`func (o *Chapter) GetBibleIdOk() (*string, bool)`

GetBibleIdOk returns a tuple with the BibleId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBibleId

`func (o *Chapter) SetBibleId(v string)`

SetBibleId sets BibleId field to given value.


### GetNumber

`func (o *Chapter) GetNumber() string`

GetNumber returns the Number field if non-nil, zero value otherwise.

### GetNumberOk

`func (o *Chapter) GetNumberOk() (*string, bool)`

GetNumberOk returns a tuple with the Number field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNumber

`func (o *Chapter) SetNumber(v string)`

SetNumber sets Number field to given value.


### GetBookId

`func (o *Chapter) GetBookId() string`

GetBookId returns the BookId field if non-nil, zero value otherwise.

### GetBookIdOk

`func (o *Chapter) GetBookIdOk() (*string, bool)`

GetBookIdOk returns a tuple with the BookId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBookId

`func (o *Chapter) SetBookId(v string)`

SetBookId sets BookId field to given value.


### GetContent

`func (o *Chapter) GetContent() string`

GetContent returns the Content field if non-nil, zero value otherwise.

### GetContentOk

`func (o *Chapter) GetContentOk() (*string, bool)`

GetContentOk returns a tuple with the Content field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContent

`func (o *Chapter) SetContent(v string)`

SetContent sets Content field to given value.


### GetReference

`func (o *Chapter) GetReference() string`

GetReference returns the Reference field if non-nil, zero value otherwise.

### GetReferenceOk

`func (o *Chapter) GetReferenceOk() (*string, bool)`

GetReferenceOk returns a tuple with the Reference field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReference

`func (o *Chapter) SetReference(v string)`

SetReference sets Reference field to given value.


### GetVerseCount

`func (o *Chapter) GetVerseCount() int32`

GetVerseCount returns the VerseCount field if non-nil, zero value otherwise.

### GetVerseCountOk

`func (o *Chapter) GetVerseCountOk() (*int32, bool)`

GetVerseCountOk returns a tuple with the VerseCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVerseCount

`func (o *Chapter) SetVerseCount(v int32)`

SetVerseCount sets VerseCount field to given value.


### GetNext

`func (o *Chapter) GetNext() ChapterNext`

GetNext returns the Next field if non-nil, zero value otherwise.

### GetNextOk

`func (o *Chapter) GetNextOk() (*ChapterNext, bool)`

GetNextOk returns a tuple with the Next field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNext

`func (o *Chapter) SetNext(v ChapterNext)`

SetNext sets Next field to given value.

### HasNext

`func (o *Chapter) HasNext() bool`

HasNext returns a boolean if a field has been set.

### GetPrevious

`func (o *Chapter) GetPrevious() ChapterNext`

GetPrevious returns the Previous field if non-nil, zero value otherwise.

### GetPreviousOk

`func (o *Chapter) GetPreviousOk() (*ChapterNext, bool)`

GetPreviousOk returns a tuple with the Previous field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrevious

`func (o *Chapter) SetPrevious(v ChapterNext)`

SetPrevious sets Previous field to given value.

### HasPrevious

`func (o *Chapter) HasPrevious() bool`

HasPrevious returns a boolean if a field has been set.

### GetCopyright

`func (o *Chapter) GetCopyright() string`

GetCopyright returns the Copyright field if non-nil, zero value otherwise.

### GetCopyrightOk

`func (o *Chapter) GetCopyrightOk() (*string, bool)`

GetCopyrightOk returns a tuple with the Copyright field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCopyright

`func (o *Chapter) SetCopyright(v string)`

SetCopyright sets Copyright field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


