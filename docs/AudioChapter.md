# AudioChapter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**BibleId** | **string** |  | 
**Number** | **string** |  | 
**BookId** | **string** |  | 
**ResourceUrl** | **string** |  | 
**Timecodes** | Pointer to [**[]AudioChapterTimecodes**](AudioChapterTimecodes.md) |  | [optional] 
**ExpiresAt** | **int32** |  | 
**Reference** | **string** |  | 
**Next** | Pointer to [**ChapterNext**](ChapterNext.md) |  | [optional] 
**Previous** | Pointer to [**ChapterNext**](ChapterNext.md) |  | [optional] 
**Copyright** | Pointer to **string** |  | [optional] 

## Methods

### NewAudioChapter

`func NewAudioChapter(id string, bibleId string, number string, bookId string, resourceUrl string, expiresAt int32, reference string, ) *AudioChapter`

NewAudioChapter instantiates a new AudioChapter object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAudioChapterWithDefaults

`func NewAudioChapterWithDefaults() *AudioChapter`

NewAudioChapterWithDefaults instantiates a new AudioChapter object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *AudioChapter) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *AudioChapter) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *AudioChapter) SetId(v string)`

SetId sets Id field to given value.


### GetBibleId

`func (o *AudioChapter) GetBibleId() string`

GetBibleId returns the BibleId field if non-nil, zero value otherwise.

### GetBibleIdOk

`func (o *AudioChapter) GetBibleIdOk() (*string, bool)`

GetBibleIdOk returns a tuple with the BibleId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBibleId

`func (o *AudioChapter) SetBibleId(v string)`

SetBibleId sets BibleId field to given value.


### GetNumber

`func (o *AudioChapter) GetNumber() string`

GetNumber returns the Number field if non-nil, zero value otherwise.

### GetNumberOk

`func (o *AudioChapter) GetNumberOk() (*string, bool)`

GetNumberOk returns a tuple with the Number field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNumber

`func (o *AudioChapter) SetNumber(v string)`

SetNumber sets Number field to given value.


### GetBookId

`func (o *AudioChapter) GetBookId() string`

GetBookId returns the BookId field if non-nil, zero value otherwise.

### GetBookIdOk

`func (o *AudioChapter) GetBookIdOk() (*string, bool)`

GetBookIdOk returns a tuple with the BookId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBookId

`func (o *AudioChapter) SetBookId(v string)`

SetBookId sets BookId field to given value.


### GetResourceUrl

`func (o *AudioChapter) GetResourceUrl() string`

GetResourceUrl returns the ResourceUrl field if non-nil, zero value otherwise.

### GetResourceUrlOk

`func (o *AudioChapter) GetResourceUrlOk() (*string, bool)`

GetResourceUrlOk returns a tuple with the ResourceUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceUrl

`func (o *AudioChapter) SetResourceUrl(v string)`

SetResourceUrl sets ResourceUrl field to given value.


### GetTimecodes

`func (o *AudioChapter) GetTimecodes() []AudioChapterTimecodes`

GetTimecodes returns the Timecodes field if non-nil, zero value otherwise.

### GetTimecodesOk

`func (o *AudioChapter) GetTimecodesOk() (*[]AudioChapterTimecodes, bool)`

GetTimecodesOk returns a tuple with the Timecodes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimecodes

`func (o *AudioChapter) SetTimecodes(v []AudioChapterTimecodes)`

SetTimecodes sets Timecodes field to given value.

### HasTimecodes

`func (o *AudioChapter) HasTimecodes() bool`

HasTimecodes returns a boolean if a field has been set.

### GetExpiresAt

`func (o *AudioChapter) GetExpiresAt() int32`

GetExpiresAt returns the ExpiresAt field if non-nil, zero value otherwise.

### GetExpiresAtOk

`func (o *AudioChapter) GetExpiresAtOk() (*int32, bool)`

GetExpiresAtOk returns a tuple with the ExpiresAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiresAt

`func (o *AudioChapter) SetExpiresAt(v int32)`

SetExpiresAt sets ExpiresAt field to given value.


### GetReference

`func (o *AudioChapter) GetReference() string`

GetReference returns the Reference field if non-nil, zero value otherwise.

### GetReferenceOk

`func (o *AudioChapter) GetReferenceOk() (*string, bool)`

GetReferenceOk returns a tuple with the Reference field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReference

`func (o *AudioChapter) SetReference(v string)`

SetReference sets Reference field to given value.


### GetNext

`func (o *AudioChapter) GetNext() ChapterNext`

GetNext returns the Next field if non-nil, zero value otherwise.

### GetNextOk

`func (o *AudioChapter) GetNextOk() (*ChapterNext, bool)`

GetNextOk returns a tuple with the Next field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNext

`func (o *AudioChapter) SetNext(v ChapterNext)`

SetNext sets Next field to given value.

### HasNext

`func (o *AudioChapter) HasNext() bool`

HasNext returns a boolean if a field has been set.

### GetPrevious

`func (o *AudioChapter) GetPrevious() ChapterNext`

GetPrevious returns the Previous field if non-nil, zero value otherwise.

### GetPreviousOk

`func (o *AudioChapter) GetPreviousOk() (*ChapterNext, bool)`

GetPreviousOk returns a tuple with the Previous field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrevious

`func (o *AudioChapter) SetPrevious(v ChapterNext)`

SetPrevious sets Previous field to given value.

### HasPrevious

`func (o *AudioChapter) HasPrevious() bool`

HasPrevious returns a boolean if a field has been set.

### GetCopyright

`func (o *AudioChapter) GetCopyright() string`

GetCopyright returns the Copyright field if non-nil, zero value otherwise.

### GetCopyrightOk

`func (o *AudioChapter) GetCopyrightOk() (*string, bool)`

GetCopyrightOk returns a tuple with the Copyright field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCopyright

`func (o *AudioChapter) SetCopyright(v string)`

SetCopyright sets Copyright field to given value.

### HasCopyright

`func (o *AudioChapter) HasCopyright() bool`

HasCopyright returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


