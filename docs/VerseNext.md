# VerseNext

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**BookId** | Pointer to **string** |  | [optional] 

## Methods

### NewVerseNext

`func NewVerseNext() *VerseNext`

NewVerseNext instantiates a new VerseNext object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVerseNextWithDefaults

`func NewVerseNextWithDefaults() *VerseNext`

NewVerseNextWithDefaults instantiates a new VerseNext object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VerseNext) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VerseNext) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VerseNext) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *VerseNext) HasId() bool`

HasId returns a boolean if a field has been set.

### GetBookId

`func (o *VerseNext) GetBookId() string`

GetBookId returns the BookId field if non-nil, zero value otherwise.

### GetBookIdOk

`func (o *VerseNext) GetBookIdOk() (*string, bool)`

GetBookIdOk returns a tuple with the BookId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBookId

`func (o *VerseNext) SetBookId(v string)`

SetBookId sets BookId field to given value.

### HasBookId

`func (o *VerseNext) HasBookId() bool`

HasBookId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


