# AudioBible

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**DblId** | **string** |  | 
**Abbreviation** | **string** |  | 
**AbbreviationLocal** | **string** |  | 
**Copyright** | **string** |  | 
**Language** | [**Language**](Language.md) |  | 
**Countries** | [**[]BibleSummaryCountries**](BibleSummaryCountries.md) |  | 
**Name** | **string** |  | 
**NameLocal** | **string** |  | 
**Description** | **string** |  | 
**DescriptionLocal** | **string** |  | 
**Info** | **string** |  | 
**Type** | **string** |  | 
**UpdatedAt** | **time.Time** |  | 
**RelatedDbl** | **string** |  | 

## Methods

### NewAudioBible

`func NewAudioBible(id string, dblId string, abbreviation string, abbreviationLocal string, copyright string, language Language, countries []BibleSummaryCountries, name string, nameLocal string, description string, descriptionLocal string, info string, type_ string, updatedAt time.Time, relatedDbl string, ) *AudioBible`

NewAudioBible instantiates a new AudioBible object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAudioBibleWithDefaults

`func NewAudioBibleWithDefaults() *AudioBible`

NewAudioBibleWithDefaults instantiates a new AudioBible object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *AudioBible) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *AudioBible) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *AudioBible) SetId(v string)`

SetId sets Id field to given value.


### GetDblId

`func (o *AudioBible) GetDblId() string`

GetDblId returns the DblId field if non-nil, zero value otherwise.

### GetDblIdOk

`func (o *AudioBible) GetDblIdOk() (*string, bool)`

GetDblIdOk returns a tuple with the DblId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDblId

`func (o *AudioBible) SetDblId(v string)`

SetDblId sets DblId field to given value.


### GetAbbreviation

`func (o *AudioBible) GetAbbreviation() string`

GetAbbreviation returns the Abbreviation field if non-nil, zero value otherwise.

### GetAbbreviationOk

`func (o *AudioBible) GetAbbreviationOk() (*string, bool)`

GetAbbreviationOk returns a tuple with the Abbreviation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAbbreviation

`func (o *AudioBible) SetAbbreviation(v string)`

SetAbbreviation sets Abbreviation field to given value.


### GetAbbreviationLocal

`func (o *AudioBible) GetAbbreviationLocal() string`

GetAbbreviationLocal returns the AbbreviationLocal field if non-nil, zero value otherwise.

### GetAbbreviationLocalOk

`func (o *AudioBible) GetAbbreviationLocalOk() (*string, bool)`

GetAbbreviationLocalOk returns a tuple with the AbbreviationLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAbbreviationLocal

`func (o *AudioBible) SetAbbreviationLocal(v string)`

SetAbbreviationLocal sets AbbreviationLocal field to given value.


### GetCopyright

`func (o *AudioBible) GetCopyright() string`

GetCopyright returns the Copyright field if non-nil, zero value otherwise.

### GetCopyrightOk

`func (o *AudioBible) GetCopyrightOk() (*string, bool)`

GetCopyrightOk returns a tuple with the Copyright field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCopyright

`func (o *AudioBible) SetCopyright(v string)`

SetCopyright sets Copyright field to given value.


### GetLanguage

`func (o *AudioBible) GetLanguage() Language`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *AudioBible) GetLanguageOk() (*Language, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *AudioBible) SetLanguage(v Language)`

SetLanguage sets Language field to given value.


### GetCountries

`func (o *AudioBible) GetCountries() []BibleSummaryCountries`

GetCountries returns the Countries field if non-nil, zero value otherwise.

### GetCountriesOk

`func (o *AudioBible) GetCountriesOk() (*[]BibleSummaryCountries, bool)`

GetCountriesOk returns a tuple with the Countries field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountries

`func (o *AudioBible) SetCountries(v []BibleSummaryCountries)`

SetCountries sets Countries field to given value.


### GetName

`func (o *AudioBible) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *AudioBible) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *AudioBible) SetName(v string)`

SetName sets Name field to given value.


### GetNameLocal

`func (o *AudioBible) GetNameLocal() string`

GetNameLocal returns the NameLocal field if non-nil, zero value otherwise.

### GetNameLocalOk

`func (o *AudioBible) GetNameLocalOk() (*string, bool)`

GetNameLocalOk returns a tuple with the NameLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNameLocal

`func (o *AudioBible) SetNameLocal(v string)`

SetNameLocal sets NameLocal field to given value.


### GetDescription

`func (o *AudioBible) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *AudioBible) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *AudioBible) SetDescription(v string)`

SetDescription sets Description field to given value.


### GetDescriptionLocal

`func (o *AudioBible) GetDescriptionLocal() string`

GetDescriptionLocal returns the DescriptionLocal field if non-nil, zero value otherwise.

### GetDescriptionLocalOk

`func (o *AudioBible) GetDescriptionLocalOk() (*string, bool)`

GetDescriptionLocalOk returns a tuple with the DescriptionLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescriptionLocal

`func (o *AudioBible) SetDescriptionLocal(v string)`

SetDescriptionLocal sets DescriptionLocal field to given value.


### GetInfo

`func (o *AudioBible) GetInfo() string`

GetInfo returns the Info field if non-nil, zero value otherwise.

### GetInfoOk

`func (o *AudioBible) GetInfoOk() (*string, bool)`

GetInfoOk returns a tuple with the Info field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInfo

`func (o *AudioBible) SetInfo(v string)`

SetInfo sets Info field to given value.


### GetType

`func (o *AudioBible) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *AudioBible) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *AudioBible) SetType(v string)`

SetType sets Type field to given value.


### GetUpdatedAt

`func (o *AudioBible) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *AudioBible) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *AudioBible) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetRelatedDbl

`func (o *AudioBible) GetRelatedDbl() string`

GetRelatedDbl returns the RelatedDbl field if non-nil, zero value otherwise.

### GetRelatedDblOk

`func (o *AudioBible) GetRelatedDblOk() (*string, bool)`

GetRelatedDblOk returns a tuple with the RelatedDbl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRelatedDbl

`func (o *AudioBible) SetRelatedDbl(v string)`

SetRelatedDbl sets RelatedDbl field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


