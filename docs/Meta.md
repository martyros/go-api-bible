# Meta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Fums** | **string** |  | 
**FumsId** | **string** |  | 
**FumsJsInclude** | **string** |  | 
**FumsJs** | **string** |  | 
**FumsNoScript** | **string** |  | 

## Methods

### NewMeta

`func NewMeta(fums string, fumsId string, fumsJsInclude string, fumsJs string, fumsNoScript string, ) *Meta`

NewMeta instantiates a new Meta object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMetaWithDefaults

`func NewMetaWithDefaults() *Meta`

NewMetaWithDefaults instantiates a new Meta object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFums

`func (o *Meta) GetFums() string`

GetFums returns the Fums field if non-nil, zero value otherwise.

### GetFumsOk

`func (o *Meta) GetFumsOk() (*string, bool)`

GetFumsOk returns a tuple with the Fums field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFums

`func (o *Meta) SetFums(v string)`

SetFums sets Fums field to given value.


### GetFumsId

`func (o *Meta) GetFumsId() string`

GetFumsId returns the FumsId field if non-nil, zero value otherwise.

### GetFumsIdOk

`func (o *Meta) GetFumsIdOk() (*string, bool)`

GetFumsIdOk returns a tuple with the FumsId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFumsId

`func (o *Meta) SetFumsId(v string)`

SetFumsId sets FumsId field to given value.


### GetFumsJsInclude

`func (o *Meta) GetFumsJsInclude() string`

GetFumsJsInclude returns the FumsJsInclude field if non-nil, zero value otherwise.

### GetFumsJsIncludeOk

`func (o *Meta) GetFumsJsIncludeOk() (*string, bool)`

GetFumsJsIncludeOk returns a tuple with the FumsJsInclude field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFumsJsInclude

`func (o *Meta) SetFumsJsInclude(v string)`

SetFumsJsInclude sets FumsJsInclude field to given value.


### GetFumsJs

`func (o *Meta) GetFumsJs() string`

GetFumsJs returns the FumsJs field if non-nil, zero value otherwise.

### GetFumsJsOk

`func (o *Meta) GetFumsJsOk() (*string, bool)`

GetFumsJsOk returns a tuple with the FumsJs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFumsJs

`func (o *Meta) SetFumsJs(v string)`

SetFumsJs sets FumsJs field to given value.


### GetFumsNoScript

`func (o *Meta) GetFumsNoScript() string`

GetFumsNoScript returns the FumsNoScript field if non-nil, zero value otherwise.

### GetFumsNoScriptOk

`func (o *Meta) GetFumsNoScriptOk() (*string, bool)`

GetFumsNoScriptOk returns a tuple with the FumsNoScript field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFumsNoScript

`func (o *Meta) SetFumsNoScript(v string)`

SetFumsNoScript sets FumsNoScript field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


