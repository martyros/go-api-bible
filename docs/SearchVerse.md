# SearchVerse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**OrgId** | **string** |  | 
**BibleId** | **string** |  | 
**BookId** | **string** |  | 
**ChapterId** | **string** |  | 
**Text** | **string** |  | 
**Reference** | Pointer to **string** |  | [optional] 

## Methods

### NewSearchVerse

`func NewSearchVerse(id string, orgId string, bibleId string, bookId string, chapterId string, text string, ) *SearchVerse`

NewSearchVerse instantiates a new SearchVerse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSearchVerseWithDefaults

`func NewSearchVerseWithDefaults() *SearchVerse`

NewSearchVerseWithDefaults instantiates a new SearchVerse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *SearchVerse) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *SearchVerse) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *SearchVerse) SetId(v string)`

SetId sets Id field to given value.


### GetOrgId

`func (o *SearchVerse) GetOrgId() string`

GetOrgId returns the OrgId field if non-nil, zero value otherwise.

### GetOrgIdOk

`func (o *SearchVerse) GetOrgIdOk() (*string, bool)`

GetOrgIdOk returns a tuple with the OrgId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrgId

`func (o *SearchVerse) SetOrgId(v string)`

SetOrgId sets OrgId field to given value.


### GetBibleId

`func (o *SearchVerse) GetBibleId() string`

GetBibleId returns the BibleId field if non-nil, zero value otherwise.

### GetBibleIdOk

`func (o *SearchVerse) GetBibleIdOk() (*string, bool)`

GetBibleIdOk returns a tuple with the BibleId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBibleId

`func (o *SearchVerse) SetBibleId(v string)`

SetBibleId sets BibleId field to given value.


### GetBookId

`func (o *SearchVerse) GetBookId() string`

GetBookId returns the BookId field if non-nil, zero value otherwise.

### GetBookIdOk

`func (o *SearchVerse) GetBookIdOk() (*string, bool)`

GetBookIdOk returns a tuple with the BookId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBookId

`func (o *SearchVerse) SetBookId(v string)`

SetBookId sets BookId field to given value.


### GetChapterId

`func (o *SearchVerse) GetChapterId() string`

GetChapterId returns the ChapterId field if non-nil, zero value otherwise.

### GetChapterIdOk

`func (o *SearchVerse) GetChapterIdOk() (*string, bool)`

GetChapterIdOk returns a tuple with the ChapterId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChapterId

`func (o *SearchVerse) SetChapterId(v string)`

SetChapterId sets ChapterId field to given value.


### GetText

`func (o *SearchVerse) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *SearchVerse) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *SearchVerse) SetText(v string)`

SetText sets Text field to given value.


### GetReference

`func (o *SearchVerse) GetReference() string`

GetReference returns the Reference field if non-nil, zero value otherwise.

### GetReferenceOk

`func (o *SearchVerse) GetReferenceOk() (*string, bool)`

GetReferenceOk returns a tuple with the Reference field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReference

`func (o *SearchVerse) SetReference(v string)`

SetReference sets Reference field to given value.

### HasReference

`func (o *SearchVerse) HasReference() bool`

HasReference returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


