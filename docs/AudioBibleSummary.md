# AudioBibleSummary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Name** | **string** |  | 
**NameLocal** | **string** |  | 
**Description** | **string** |  | 
**DescriptionLocal** | **string** |  | 

## Methods

### NewAudioBibleSummary

`func NewAudioBibleSummary(id string, name string, nameLocal string, description string, descriptionLocal string, ) *AudioBibleSummary`

NewAudioBibleSummary instantiates a new AudioBibleSummary object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAudioBibleSummaryWithDefaults

`func NewAudioBibleSummaryWithDefaults() *AudioBibleSummary`

NewAudioBibleSummaryWithDefaults instantiates a new AudioBibleSummary object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *AudioBibleSummary) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *AudioBibleSummary) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *AudioBibleSummary) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *AudioBibleSummary) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *AudioBibleSummary) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *AudioBibleSummary) SetName(v string)`

SetName sets Name field to given value.


### GetNameLocal

`func (o *AudioBibleSummary) GetNameLocal() string`

GetNameLocal returns the NameLocal field if non-nil, zero value otherwise.

### GetNameLocalOk

`func (o *AudioBibleSummary) GetNameLocalOk() (*string, bool)`

GetNameLocalOk returns a tuple with the NameLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNameLocal

`func (o *AudioBibleSummary) SetNameLocal(v string)`

SetNameLocal sets NameLocal field to given value.


### GetDescription

`func (o *AudioBibleSummary) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *AudioBibleSummary) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *AudioBibleSummary) SetDescription(v string)`

SetDescription sets Description field to given value.


### GetDescriptionLocal

`func (o *AudioBibleSummary) GetDescriptionLocal() string`

GetDescriptionLocal returns the DescriptionLocal field if non-nil, zero value otherwise.

### GetDescriptionLocalOk

`func (o *AudioBibleSummary) GetDescriptionLocalOk() (*string, bool)`

GetDescriptionLocalOk returns a tuple with the DescriptionLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescriptionLocal

`func (o *AudioBibleSummary) SetDescriptionLocal(v string)`

SetDescriptionLocal sets DescriptionLocal field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


