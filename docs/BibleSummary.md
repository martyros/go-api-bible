# BibleSummary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**DblId** | **string** |  | 
**Abbreviation** | **string** |  | 
**AbbreviationLocal** | **string** |  | 
**Language** | [**Language**](Language.md) |  | 
**Countries** | [**[]BibleSummaryCountries**](BibleSummaryCountries.md) |  | 
**Name** | **string** |  | 
**NameLocal** | **string** |  | 
**Description** | **string** |  | 
**DescriptionLocal** | **string** |  | 
**RelatedDbl** | **string** |  | 
**Type** | **string** |  | 
**UpdatedAt** | **time.Time** |  | 
**AudioBibles** | Pointer to [**[]AudioBibleSummary**](AudioBibleSummary.md) |  | [optional] 

## Methods

### NewBibleSummary

`func NewBibleSummary(id string, dblId string, abbreviation string, abbreviationLocal string, language Language, countries []BibleSummaryCountries, name string, nameLocal string, description string, descriptionLocal string, relatedDbl string, type_ string, updatedAt time.Time, ) *BibleSummary`

NewBibleSummary instantiates a new BibleSummary object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBibleSummaryWithDefaults

`func NewBibleSummaryWithDefaults() *BibleSummary`

NewBibleSummaryWithDefaults instantiates a new BibleSummary object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *BibleSummary) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *BibleSummary) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *BibleSummary) SetId(v string)`

SetId sets Id field to given value.


### GetDblId

`func (o *BibleSummary) GetDblId() string`

GetDblId returns the DblId field if non-nil, zero value otherwise.

### GetDblIdOk

`func (o *BibleSummary) GetDblIdOk() (*string, bool)`

GetDblIdOk returns a tuple with the DblId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDblId

`func (o *BibleSummary) SetDblId(v string)`

SetDblId sets DblId field to given value.


### GetAbbreviation

`func (o *BibleSummary) GetAbbreviation() string`

GetAbbreviation returns the Abbreviation field if non-nil, zero value otherwise.

### GetAbbreviationOk

`func (o *BibleSummary) GetAbbreviationOk() (*string, bool)`

GetAbbreviationOk returns a tuple with the Abbreviation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAbbreviation

`func (o *BibleSummary) SetAbbreviation(v string)`

SetAbbreviation sets Abbreviation field to given value.


### GetAbbreviationLocal

`func (o *BibleSummary) GetAbbreviationLocal() string`

GetAbbreviationLocal returns the AbbreviationLocal field if non-nil, zero value otherwise.

### GetAbbreviationLocalOk

`func (o *BibleSummary) GetAbbreviationLocalOk() (*string, bool)`

GetAbbreviationLocalOk returns a tuple with the AbbreviationLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAbbreviationLocal

`func (o *BibleSummary) SetAbbreviationLocal(v string)`

SetAbbreviationLocal sets AbbreviationLocal field to given value.


### GetLanguage

`func (o *BibleSummary) GetLanguage() Language`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *BibleSummary) GetLanguageOk() (*Language, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *BibleSummary) SetLanguage(v Language)`

SetLanguage sets Language field to given value.


### GetCountries

`func (o *BibleSummary) GetCountries() []BibleSummaryCountries`

GetCountries returns the Countries field if non-nil, zero value otherwise.

### GetCountriesOk

`func (o *BibleSummary) GetCountriesOk() (*[]BibleSummaryCountries, bool)`

GetCountriesOk returns a tuple with the Countries field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountries

`func (o *BibleSummary) SetCountries(v []BibleSummaryCountries)`

SetCountries sets Countries field to given value.


### GetName

`func (o *BibleSummary) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *BibleSummary) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *BibleSummary) SetName(v string)`

SetName sets Name field to given value.


### GetNameLocal

`func (o *BibleSummary) GetNameLocal() string`

GetNameLocal returns the NameLocal field if non-nil, zero value otherwise.

### GetNameLocalOk

`func (o *BibleSummary) GetNameLocalOk() (*string, bool)`

GetNameLocalOk returns a tuple with the NameLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNameLocal

`func (o *BibleSummary) SetNameLocal(v string)`

SetNameLocal sets NameLocal field to given value.


### GetDescription

`func (o *BibleSummary) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *BibleSummary) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *BibleSummary) SetDescription(v string)`

SetDescription sets Description field to given value.


### GetDescriptionLocal

`func (o *BibleSummary) GetDescriptionLocal() string`

GetDescriptionLocal returns the DescriptionLocal field if non-nil, zero value otherwise.

### GetDescriptionLocalOk

`func (o *BibleSummary) GetDescriptionLocalOk() (*string, bool)`

GetDescriptionLocalOk returns a tuple with the DescriptionLocal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescriptionLocal

`func (o *BibleSummary) SetDescriptionLocal(v string)`

SetDescriptionLocal sets DescriptionLocal field to given value.


### GetRelatedDbl

`func (o *BibleSummary) GetRelatedDbl() string`

GetRelatedDbl returns the RelatedDbl field if non-nil, zero value otherwise.

### GetRelatedDblOk

`func (o *BibleSummary) GetRelatedDblOk() (*string, bool)`

GetRelatedDblOk returns a tuple with the RelatedDbl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRelatedDbl

`func (o *BibleSummary) SetRelatedDbl(v string)`

SetRelatedDbl sets RelatedDbl field to given value.


### GetType

`func (o *BibleSummary) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *BibleSummary) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *BibleSummary) SetType(v string)`

SetType sets Type field to given value.


### GetUpdatedAt

`func (o *BibleSummary) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *BibleSummary) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *BibleSummary) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetAudioBibles

`func (o *BibleSummary) GetAudioBibles() []AudioBibleSummary`

GetAudioBibles returns the AudioBibles field if non-nil, zero value otherwise.

### GetAudioBiblesOk

`func (o *BibleSummary) GetAudioBiblesOk() (*[]AudioBibleSummary, bool)`

GetAudioBiblesOk returns a tuple with the AudioBibles field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAudioBibles

`func (o *BibleSummary) SetAudioBibles(v []AudioBibleSummary)`

SetAudioBibles sets AudioBibles field to given value.

### HasAudioBibles

`func (o *BibleSummary) HasAudioBibles() bool`

HasAudioBibles returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


