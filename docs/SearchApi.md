# \SearchApi

All URIs are relative to *https://api.scripture.api.bible*

Method | HTTP request | Description
------------- | ------------- | -------------
[**SearchBible**](SearchApi.md#SearchBible) | **Get** /v1/bibles/{bibleId}/search | 



## SearchBible

> InlineResponse20011 SearchBible(ctx, bibleId).Query(query).Limit(limit).Offset(offset).Sort(sort).Range_(range_).Fuzziness(fuzziness).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bibleId := "bibleId_example" // string | Id of Bible to search
    query := "query_example" // string | Search keywords or passage reference.  Supported wildcards are * and ?.   The * wildcard matches any character sequence (e.g. searching for \"wo*d\" finds text such as \"word\", \"world\", and \"worshipped\").   The ? wildcard matches any matches any single character (e.g. searching for \"l?ve\" finds text such as \"live\" and \"love\").  (optional)
    limit := int32(56) // int32 | Integer limit for how many matching results to return. Default is 10. (optional)
    offset := int32(56) // int32 | Offset for search results. Used to paginate results (optional)
    sort := "sort_example" // string | Sort order of results.  Supported values are `relevance` (default), `canonical` and `reverse-canonical` (optional) (default to "relevance")
    range_ := "range__example" // string | One or more, comma seperated, passage ids (book, chapter, verse) which the search will be limited to.  (i.e. gen.1,gen.5 or gen-num or gen.1.1-gen.3.5)  (optional)
    fuzziness := "fuzziness_example" // string | Sets the fuzziness of a search to account for misspellings. Values can be 0, 1, 2, or AUTO. Defaults to AUTO which varies depending on the  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.SearchApi.SearchBible(context.Background(), bibleId).Query(query).Limit(limit).Offset(offset).Sort(sort).Range_(range_).Fuzziness(fuzziness).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `SearchApi.SearchBible``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SearchBible`: InlineResponse20011
    fmt.Fprintf(os.Stdout, "Response from `SearchApi.SearchBible`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**bibleId** | **string** | Id of Bible to search | 

### Other Parameters

Other parameters are passed through a pointer to a apiSearchBibleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **query** | **string** | Search keywords or passage reference.  Supported wildcards are * and ?.   The * wildcard matches any character sequence (e.g. searching for \&quot;wo*d\&quot; finds text such as \&quot;word\&quot;, \&quot;world\&quot;, and \&quot;worshipped\&quot;).   The ? wildcard matches any matches any single character (e.g. searching for \&quot;l?ve\&quot; finds text such as \&quot;live\&quot; and \&quot;love\&quot;).  | 
 **limit** | **int32** | Integer limit for how many matching results to return. Default is 10. | 
 **offset** | **int32** | Offset for search results. Used to paginate results | 
 **sort** | **string** | Sort order of results.  Supported values are &#x60;relevance&#x60; (default), &#x60;canonical&#x60; and &#x60;reverse-canonical&#x60; | [default to &quot;relevance&quot;]
 **range_** | **string** | One or more, comma seperated, passage ids (book, chapter, verse) which the search will be limited to.  (i.e. gen.1,gen.5 or gen-num or gen.1.1-gen.3.5)  | 
 **fuzziness** | **string** | Sets the fuzziness of a search to account for misspellings. Values can be 0, 1, 2, or AUTO. Defaults to AUTO which varies depending on the  | 

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

