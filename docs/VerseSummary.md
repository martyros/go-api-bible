# VerseSummary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**OrgId** | **string** |  | 
**BibleId** | **string** |  | 
**BookId** | **string** |  | 
**ChapterId** | **string** |  | 
**Reference** | **string** |  | 

## Methods

### NewVerseSummary

`func NewVerseSummary(id string, orgId string, bibleId string, bookId string, chapterId string, reference string, ) *VerseSummary`

NewVerseSummary instantiates a new VerseSummary object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVerseSummaryWithDefaults

`func NewVerseSummaryWithDefaults() *VerseSummary`

NewVerseSummaryWithDefaults instantiates a new VerseSummary object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VerseSummary) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VerseSummary) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VerseSummary) SetId(v string)`

SetId sets Id field to given value.


### GetOrgId

`func (o *VerseSummary) GetOrgId() string`

GetOrgId returns the OrgId field if non-nil, zero value otherwise.

### GetOrgIdOk

`func (o *VerseSummary) GetOrgIdOk() (*string, bool)`

GetOrgIdOk returns a tuple with the OrgId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrgId

`func (o *VerseSummary) SetOrgId(v string)`

SetOrgId sets OrgId field to given value.


### GetBibleId

`func (o *VerseSummary) GetBibleId() string`

GetBibleId returns the BibleId field if non-nil, zero value otherwise.

### GetBibleIdOk

`func (o *VerseSummary) GetBibleIdOk() (*string, bool)`

GetBibleIdOk returns a tuple with the BibleId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBibleId

`func (o *VerseSummary) SetBibleId(v string)`

SetBibleId sets BibleId field to given value.


### GetBookId

`func (o *VerseSummary) GetBookId() string`

GetBookId returns the BookId field if non-nil, zero value otherwise.

### GetBookIdOk

`func (o *VerseSummary) GetBookIdOk() (*string, bool)`

GetBookIdOk returns a tuple with the BookId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBookId

`func (o *VerseSummary) SetBookId(v string)`

SetBookId sets BookId field to given value.


### GetChapterId

`func (o *VerseSummary) GetChapterId() string`

GetChapterId returns the ChapterId field if non-nil, zero value otherwise.

### GetChapterIdOk

`func (o *VerseSummary) GetChapterIdOk() (*string, bool)`

GetChapterIdOk returns a tuple with the ChapterId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChapterId

`func (o *VerseSummary) SetChapterId(v string)`

SetChapterId sets ChapterId field to given value.


### GetReference

`func (o *VerseSummary) GetReference() string`

GetReference returns the Reference field if non-nil, zero value otherwise.

### GetReferenceOk

`func (o *VerseSummary) GetReferenceOk() (*string, bool)`

GetReferenceOk returns a tuple with the Reference field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReference

`func (o *VerseSummary) SetReference(v string)`

SetReference sets Reference field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


