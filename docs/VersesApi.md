# \VersesApi

All URIs are relative to *https://api.scripture.api.bible*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetVerse**](VersesApi.md#GetVerse) | **Get** /v1/bibles/{bibleId}/verses/{verseId} | 
[**GetVerses**](VersesApi.md#GetVerses) | **Get** /v1/bibles/{bibleId}/chapters/{chapterId}/verses | 



## GetVerse

> InlineResponse20010 GetVerse(ctx, bibleId, verseId).ContentType(contentType).IncludeNotes(includeNotes).IncludeTitles(includeTitles).IncludeChapterNumbers(includeChapterNumbers).IncludeVerseNumbers(includeVerseNumbers).IncludeVerseSpans(includeVerseSpans).Parallels(parallels).UseOrgId(useOrgId).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bibleId := "bibleId_example" // string | Id of Bible for passage
    verseId := "verseId_example" // string | String reference id for the requested verse.
    contentType := "contentType_example" // string | Content type to be returned in the content property.  Supported values are `html` (default), `json` (beta), and `text` (beta) (optional) (default to "html")
    includeNotes := true // bool | Include footnotes in content (optional) (default to false)
    includeTitles := true // bool | Include section titles in content (optional) (default to true)
    includeChapterNumbers := true // bool | Include chapter numbers in content (optional) (default to false)
    includeVerseNumbers := true // bool | Include verse numbers in content. (optional) (default to true)
    includeVerseSpans := true // bool | Include spans that wrap verse numbers and verse text for bible content. (optional) (default to false)
    parallels := "parallels_example" // string | Comma delimited list of bibleIds to include (optional)
    useOrgId := true // bool | Use the supplied id(s) to match the verseOrgId instead of the verseId (optional) (default to false)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.VersesApi.GetVerse(context.Background(), bibleId, verseId).ContentType(contentType).IncludeNotes(includeNotes).IncludeTitles(includeTitles).IncludeChapterNumbers(includeChapterNumbers).IncludeVerseNumbers(includeVerseNumbers).IncludeVerseSpans(includeVerseSpans).Parallels(parallels).UseOrgId(useOrgId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VersesApi.GetVerse``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetVerse`: InlineResponse20010
    fmt.Fprintf(os.Stdout, "Response from `VersesApi.GetVerse`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**bibleId** | **string** | Id of Bible for passage | 
**verseId** | **string** | String reference id for the requested verse. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetVerseRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **contentType** | **string** | Content type to be returned in the content property.  Supported values are &#x60;html&#x60; (default), &#x60;json&#x60; (beta), and &#x60;text&#x60; (beta) | [default to &quot;html&quot;]
 **includeNotes** | **bool** | Include footnotes in content | [default to false]
 **includeTitles** | **bool** | Include section titles in content | [default to true]
 **includeChapterNumbers** | **bool** | Include chapter numbers in content | [default to false]
 **includeVerseNumbers** | **bool** | Include verse numbers in content. | [default to true]
 **includeVerseSpans** | **bool** | Include spans that wrap verse numbers and verse text for bible content. | [default to false]
 **parallels** | **string** | Comma delimited list of bibleIds to include | 
 **useOrgId** | **bool** | Use the supplied id(s) to match the verseOrgId instead of the verseId | [default to false]

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVerses

> InlineResponse2009 GetVerses(ctx, bibleId, chapterId).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bibleId := "bibleId_example" // string | Id of Bible whose Verses to fetch
    chapterId := "chapterId_example" // string | Id of the Chapter whose Verses to fetch

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.VersesApi.GetVerses(context.Background(), bibleId, chapterId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VersesApi.GetVerses``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetVerses`: InlineResponse2009
    fmt.Fprintf(os.Stdout, "Response from `VersesApi.GetVerses`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**bibleId** | **string** | Id of Bible whose Verses to fetch | 
**chapterId** | **string** | Id of the Chapter whose Verses to fetch | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetVersesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**InlineResponse2009**](InlineResponse2009.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

