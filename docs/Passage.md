# Passage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**BibleId** | **string** |  | 
**OrgId** | **string** |  | 
**Content** | **string** |  | 
**Reference** | **string** |  | 
**VerseCount** | **int32** |  | 
**Copyright** | **string** |  | 

## Methods

### NewPassage

`func NewPassage(id string, bibleId string, orgId string, content string, reference string, verseCount int32, copyright string, ) *Passage`

NewPassage instantiates a new Passage object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPassageWithDefaults

`func NewPassageWithDefaults() *Passage`

NewPassageWithDefaults instantiates a new Passage object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Passage) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Passage) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Passage) SetId(v string)`

SetId sets Id field to given value.


### GetBibleId

`func (o *Passage) GetBibleId() string`

GetBibleId returns the BibleId field if non-nil, zero value otherwise.

### GetBibleIdOk

`func (o *Passage) GetBibleIdOk() (*string, bool)`

GetBibleIdOk returns a tuple with the BibleId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBibleId

`func (o *Passage) SetBibleId(v string)`

SetBibleId sets BibleId field to given value.


### GetOrgId

`func (o *Passage) GetOrgId() string`

GetOrgId returns the OrgId field if non-nil, zero value otherwise.

### GetOrgIdOk

`func (o *Passage) GetOrgIdOk() (*string, bool)`

GetOrgIdOk returns a tuple with the OrgId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrgId

`func (o *Passage) SetOrgId(v string)`

SetOrgId sets OrgId field to given value.


### GetContent

`func (o *Passage) GetContent() string`

GetContent returns the Content field if non-nil, zero value otherwise.

### GetContentOk

`func (o *Passage) GetContentOk() (*string, bool)`

GetContentOk returns a tuple with the Content field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContent

`func (o *Passage) SetContent(v string)`

SetContent sets Content field to given value.


### GetReference

`func (o *Passage) GetReference() string`

GetReference returns the Reference field if non-nil, zero value otherwise.

### GetReferenceOk

`func (o *Passage) GetReferenceOk() (*string, bool)`

GetReferenceOk returns a tuple with the Reference field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReference

`func (o *Passage) SetReference(v string)`

SetReference sets Reference field to given value.


### GetVerseCount

`func (o *Passage) GetVerseCount() int32`

GetVerseCount returns the VerseCount field if non-nil, zero value otherwise.

### GetVerseCountOk

`func (o *Passage) GetVerseCountOk() (*int32, bool)`

GetVerseCountOk returns a tuple with the VerseCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVerseCount

`func (o *Passage) SetVerseCount(v int32)`

SetVerseCount sets VerseCount field to given value.


### GetCopyright

`func (o *Passage) GetCopyright() string`

GetCopyright returns the Copyright field if non-nil, zero value otherwise.

### GetCopyrightOk

`func (o *Passage) GetCopyrightOk() (*string, bool)`

GetCopyrightOk returns a tuple with the Copyright field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCopyright

`func (o *Passage) SetCopyright(v string)`

SetCopyright sets Copyright field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


