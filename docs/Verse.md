# Verse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**OrgId** | **string** |  | 
**BibleId** | **string** |  | 
**BookId** | **string** |  | 
**ChapterId** | **string** |  | 
**Content** | **string** |  | 
**Reference** | **string** |  | 
**VerseCount** | **int32** |  | 
**Copyright** | **string** |  | 
**Next** | Pointer to [**VerseNext**](VerseNext.md) |  | [optional] 
**Previous** | Pointer to [**VerseNext**](VerseNext.md) |  | [optional] 

## Methods

### NewVerse

`func NewVerse(id string, orgId string, bibleId string, bookId string, chapterId string, content string, reference string, verseCount int32, copyright string, ) *Verse`

NewVerse instantiates a new Verse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVerseWithDefaults

`func NewVerseWithDefaults() *Verse`

NewVerseWithDefaults instantiates a new Verse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Verse) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Verse) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Verse) SetId(v string)`

SetId sets Id field to given value.


### GetOrgId

`func (o *Verse) GetOrgId() string`

GetOrgId returns the OrgId field if non-nil, zero value otherwise.

### GetOrgIdOk

`func (o *Verse) GetOrgIdOk() (*string, bool)`

GetOrgIdOk returns a tuple with the OrgId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrgId

`func (o *Verse) SetOrgId(v string)`

SetOrgId sets OrgId field to given value.


### GetBibleId

`func (o *Verse) GetBibleId() string`

GetBibleId returns the BibleId field if non-nil, zero value otherwise.

### GetBibleIdOk

`func (o *Verse) GetBibleIdOk() (*string, bool)`

GetBibleIdOk returns a tuple with the BibleId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBibleId

`func (o *Verse) SetBibleId(v string)`

SetBibleId sets BibleId field to given value.


### GetBookId

`func (o *Verse) GetBookId() string`

GetBookId returns the BookId field if non-nil, zero value otherwise.

### GetBookIdOk

`func (o *Verse) GetBookIdOk() (*string, bool)`

GetBookIdOk returns a tuple with the BookId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBookId

`func (o *Verse) SetBookId(v string)`

SetBookId sets BookId field to given value.


### GetChapterId

`func (o *Verse) GetChapterId() string`

GetChapterId returns the ChapterId field if non-nil, zero value otherwise.

### GetChapterIdOk

`func (o *Verse) GetChapterIdOk() (*string, bool)`

GetChapterIdOk returns a tuple with the ChapterId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChapterId

`func (o *Verse) SetChapterId(v string)`

SetChapterId sets ChapterId field to given value.


### GetContent

`func (o *Verse) GetContent() string`

GetContent returns the Content field if non-nil, zero value otherwise.

### GetContentOk

`func (o *Verse) GetContentOk() (*string, bool)`

GetContentOk returns a tuple with the Content field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContent

`func (o *Verse) SetContent(v string)`

SetContent sets Content field to given value.


### GetReference

`func (o *Verse) GetReference() string`

GetReference returns the Reference field if non-nil, zero value otherwise.

### GetReferenceOk

`func (o *Verse) GetReferenceOk() (*string, bool)`

GetReferenceOk returns a tuple with the Reference field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReference

`func (o *Verse) SetReference(v string)`

SetReference sets Reference field to given value.


### GetVerseCount

`func (o *Verse) GetVerseCount() int32`

GetVerseCount returns the VerseCount field if non-nil, zero value otherwise.

### GetVerseCountOk

`func (o *Verse) GetVerseCountOk() (*int32, bool)`

GetVerseCountOk returns a tuple with the VerseCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVerseCount

`func (o *Verse) SetVerseCount(v int32)`

SetVerseCount sets VerseCount field to given value.


### GetCopyright

`func (o *Verse) GetCopyright() string`

GetCopyright returns the Copyright field if non-nil, zero value otherwise.

### GetCopyrightOk

`func (o *Verse) GetCopyrightOk() (*string, bool)`

GetCopyrightOk returns a tuple with the Copyright field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCopyright

`func (o *Verse) SetCopyright(v string)`

SetCopyright sets Copyright field to given value.


### GetNext

`func (o *Verse) GetNext() VerseNext`

GetNext returns the Next field if non-nil, zero value otherwise.

### GetNextOk

`func (o *Verse) GetNextOk() (*VerseNext, bool)`

GetNextOk returns a tuple with the Next field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNext

`func (o *Verse) SetNext(v VerseNext)`

SetNext sets Next field to given value.

### HasNext

`func (o *Verse) HasNext() bool`

HasNext returns a boolean if a field has been set.

### GetPrevious

`func (o *Verse) GetPrevious() VerseNext`

GetPrevious returns the Previous field if non-nil, zero value otherwise.

### GetPreviousOk

`func (o *Verse) GetPreviousOk() (*VerseNext, bool)`

GetPreviousOk returns a tuple with the Previous field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrevious

`func (o *Verse) SetPrevious(v VerseNext)`

SetPrevious sets Previous field to given value.

### HasPrevious

`func (o *Verse) HasPrevious() bool`

HasPrevious returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


