# \PassagesApi

All URIs are relative to *https://api.scripture.api.bible*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetPassage**](PassagesApi.md#GetPassage) | **Get** /v1/bibles/{bibleId}/passages/{passageId} | 



## GetPassage

> InlineResponse2006 GetPassage(ctx, bibleId, passageId).ContentType(contentType).IncludeNotes(includeNotes).IncludeTitles(includeTitles).IncludeChapterNumbers(includeChapterNumbers).IncludeVerseNumbers(includeVerseNumbers).IncludeVerseSpans(includeVerseSpans).Parallels(parallels).UseOrgId(useOrgId).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bibleId := "bibleId_example" // string | Id of Bible for passage
    passageId := "passageId_example" // string | String reference id for the requested passage.
    contentType := "contentType_example" // string | Content type to be returned in the content property.  Supported values are `html` (default), `json` (beta), and `text` (beta) (optional) (default to "html")
    includeNotes := true // bool | Include footnotes in content (optional) (default to false)
    includeTitles := true // bool | Include section titles in content (optional) (default to true)
    includeChapterNumbers := true // bool | Include chapter numbers in content (optional) (default to false)
    includeVerseNumbers := true // bool | Include verse numbers in content. (optional) (default to true)
    includeVerseSpans := true // bool | Include spans that wrap verse numbers and verse text for bible content. (optional) (default to false)
    parallels := "parallels_example" // string | Comma delimited list of bibleIds to include (optional)
    useOrgId := true // bool | Use the supplied id(s) to match the verseOrgId instead of the verseId (optional) (default to false)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.PassagesApi.GetPassage(context.Background(), bibleId, passageId).ContentType(contentType).IncludeNotes(includeNotes).IncludeTitles(includeTitles).IncludeChapterNumbers(includeChapterNumbers).IncludeVerseNumbers(includeVerseNumbers).IncludeVerseSpans(includeVerseSpans).Parallels(parallels).UseOrgId(useOrgId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `PassagesApi.GetPassage``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetPassage`: InlineResponse2006
    fmt.Fprintf(os.Stdout, "Response from `PassagesApi.GetPassage`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**bibleId** | **string** | Id of Bible for passage | 
**passageId** | **string** | String reference id for the requested passage. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetPassageRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **contentType** | **string** | Content type to be returned in the content property.  Supported values are &#x60;html&#x60; (default), &#x60;json&#x60; (beta), and &#x60;text&#x60; (beta) | [default to &quot;html&quot;]
 **includeNotes** | **bool** | Include footnotes in content | [default to false]
 **includeTitles** | **bool** | Include section titles in content | [default to true]
 **includeChapterNumbers** | **bool** | Include chapter numbers in content | [default to false]
 **includeVerseNumbers** | **bool** | Include verse numbers in content. | [default to true]
 **includeVerseSpans** | **bool** | Include spans that wrap verse numbers and verse text for bible content. | [default to false]
 **parallels** | **string** | Comma delimited list of bibleIds to include | 
 **useOrgId** | **bool** | Use the supplied id(s) to match the verseOrgId instead of the verseId | [default to false]

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

